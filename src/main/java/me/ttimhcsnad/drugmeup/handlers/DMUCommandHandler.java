package me.ttimhcsnad.drugmeup.handlers;

import java.util.Arrays;
import java.util.List;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Handler for handling the main "drugmeup" command
 */
public class DMUCommandHandler implements CommandExecutor {

  private DrugMeUp plugin;

  /**
   * Setup the DMU main command handler
   *
   * @param plugin The {@link DrugMeUp} instance
   */
  public DMUCommandHandler(DrugMeUp plugin) {
    this.plugin = plugin;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
    String message;
    if (command.getName().equals("drugmeup")) {
      if (args.length > 0) {
        switch (args[0]) {
          case "reload":
            if (sender.hasPermission("drugmeup.reload")) {
              plugin.reload();
              message = "&c[DrugMeUp] &ereloaded!";
              sender.sendMessage(Utils.parseMessage(message));
            } else {
              Utils.noPermission(sender);
            }
            break;
          case "sober":
            if (args.length >= 2) {
              Player player = Bukkit.getOnlinePlayers().stream()
                  .filter(p -> p.getName().equals(args[1])).findFirst().orElse(null);
              if (player != null) {
                if (sender.hasPermission("drugmeup.sober.others")) {
                  plugin.getUserManager().getDrugUser(player).soberAll();
                  message = "&c[DrugMeUp] &a@p &ehas been sobered!";
                  sender.sendMessage(Utils.parseMessage(message, player));
                } else {
                  Utils.noPermission(player);
                }
              } else {
                Utils.invalidPlayer(sender, args[1]);
              }
            } else {
              if (sender instanceof Player) {
                if (sender.hasPermission("drugmeup.sober.self")) {
                  DrugUser user = plugin.getUserManager().getDrugUser((Player) sender);
                  if (user != null) {
                    user.soberAll();
                  }
                } else {
                  Utils.noPermission((Player) sender);
                }
              } else {
                sender.sendMessage(
                    Utils.parseMessage("&c[DrugMeUp] You must specify a player to sober!"));
              }
            }
            break;
          case "newdrug":
          case "createdrug":
          case "create":
            if (sender.hasPermission("drugmeup.create")) {
              if (args.length == 3) {
                if (sender.hasPermission("drugmeup.create")) {
                  try {
                    plugin.getFileManager().setupDrug(args[1], args[2]);
                  } catch (Exception ex) {
                    message = "&c[DrugMeUp] " + ex.getMessage();
                    sender.sendMessage(Utils.parseMessage(message));
                  }
                }
              } else {
                message = "&c[DrugMeUp] Invalid arguments!";
                sender.sendMessage(Utils.parseMessage(message));
                message = "&c[DrugMeUp] Usage: " + command.getName() + " " + args[0]
                    + " [drugname] [itemname] [use/consume]";
                sender.sendMessage(Utils.parseMessage(message));
              }
            } else {
              Utils.noPermission(sender);
            }
            break;
          case "addeffect":
          case "effect":
            if (sender.hasPermission("drugmeup.create")) {
              if (args.length >= 4) {
                String drugName = args[1];
                List<String> types = Arrays.asList("potion", "particle", "sound", "command");
                String type = args[2].toLowerCase();
                String effectName = args[3].toLowerCase();
                if (types.contains(type)) {
                  try {
                    switch (type) {
                      case "potion":
                        plugin.getFileManager().addPotionEffect(drugName, effectName);
                        break;
                      case "particle":
                        plugin.getFileManager().addParticleEffect(drugName, effectName);
                        break;
                      case "sound":
                        plugin.getFileManager().addSoundEffect(drugName, effectName);
                        break;
                      case "command":
                        plugin.getFileManager()
                            .addCommand(drugName, Utils.join(args, " ", 3, args.length));
                        break;
                    }
                  } catch (Exception ex) {
                    message = "&c[DrugMeUp] " + ex.getMessage();
                    sender.sendMessage(Utils.parseMessage(message));
                  }
                  break;
                }
              }
              message = "&c[DrugMeUp] Invalid arguments!";
              sender.sendMessage(Utils.parseMessage(message));
              message = "&c[DrugMeUp] Usage: " + command.getName() + " " + args[0]
                  + " [drugname] [potion/particle/sound/command] [name]";
              sender.sendMessage(Utils.parseMessage(message));
            } else {
              Utils.noPermission(sender);
            }
            break;
          default:
            message = "&c[DrugMeUp] Invalid arguments!";
            sender.sendMessage(Utils.parseMessage(message));
            message = "&c[DrugMeUp] " + plugin.getCommand("drugmeup").getUsage();
            sender.sendMessage(Utils.parseMessage(message));
            break;
        }
      }
    } else if (command.getName().equalsIgnoreCase("sober")) {

    }

    return true;
  }

}
