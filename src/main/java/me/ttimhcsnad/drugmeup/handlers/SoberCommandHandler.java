package me.ttimhcsnad.drugmeup.handlers;

import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * The Handler for handling the "sober" command
 */
public class SoberCommandHandler implements CommandExecutor {

  private DrugMeUp plugin;

  /**
   * Setup Sober CommandHandler\
   *
   * @param plugin The {@link DrugMeUp} instance
   */
  public SoberCommandHandler(DrugMeUp plugin) {
    this.plugin = plugin;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
    if (cmd.getName().equalsIgnoreCase("sober")) {
      switch (args.length) {
        case 0:
          if (sender.hasPermission("drugmeup.sober.self")) {
            if (sender instanceof Player) {
              DrugUser user = plugin.getUserManager().getDrugUser((Player) sender);
              if (user != null) {
                user.soberAll();
              } else {
                sender.sendMessage(Utils.parseMessage("&c[DrugMeUp] You are not on any drugs!"));
              }
            } else {
              sender.sendMessage(
                  Utils.parseMessage("&c[DrugMeUp] You must specify a player to sober!"));
            }
          } else {
            Utils.noPermission(sender);
          }
          break;
        case 1:
          Player player = Bukkit.getOnlinePlayers().stream()
              .filter(p -> p.getName().equals(args[0])).findFirst().orElse(null);
          if (player != null) {
            if (sender.hasPermission("drugmeup.sober.others")) {
              DrugUser user = plugin.getUserManager().getDrugUser(player);
              if (user != null) {
                user.soberAll();
                sender.sendMessage(
                    Utils.parseMessage("&c[DrugMeUp] &a@p &ehas been sobered!", player));
              } else {
                sender.sendMessage(
                    Utils.parseMessage("&c[DrugMeUp] @p is not on any drugs!", args[0]));
              }
            } else {
              Utils.noPermission(player);
            }
          } else {
            Utils.invalidPlayer(sender, args[0]);
          }
          break;
        default:
          sender.sendMessage(Utils.parseMessage("&c[DrugMeUp] Invalid Arguments!"));
          sender.sendMessage(
              Utils.parseMessage("&c[DrugMeUp] " + plugin.getCommand("sober").getUsage()));
          break;
      }
    }
    return true;
  }
}
