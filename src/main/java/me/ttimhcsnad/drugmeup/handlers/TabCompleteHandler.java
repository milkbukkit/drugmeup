package me.ttimhcsnad.drugmeup.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.HumanEntity;

public class TabCompleteHandler implements TabCompleter {

  private DrugMeUp plugin;

  public TabCompleteHandler(DrugMeUp plugin) {
    this.plugin = plugin;
  }

  /**
   * Static method to allow tab-completion handling
   *
   * @param sender The {@link CommandSender} involved
   * @param command The {@link Command} executed
   * @param label The label of the command
   * @param args An array of arguments involved in the command
   * @return A collection of valid tab complections
   */
  public List<String> onTabComplete(CommandSender sender, Command command, String label,
      String args[]) {
    if (args.length == 1) {
      return Stream.of("reload", "sober", "create", "addeffect")
          .filter(arg -> arg.contains(args[0]))
          .collect(Collectors.toList());
    }

    switch (args[0].toLowerCase()) {
      case "sober":
        if (args.length == 2) {
          return Bukkit.getOnlinePlayers().stream()
              .map(HumanEntity::getName)
              .filter(name -> name.contains(args[1]))
              .collect(Collectors.toList());
        }
        break;
      case "create":
        if (args.length == 3) {
          return Utils.MATERIAL_NAMES.stream()
              .filter(name -> name.contains(args[2]))
              .collect(Collectors.toList());
        }
        break;
      case "effect":
      case "addeffect":
        if (args.length == 2) {
          return Utils.getDrugFileNames(plugin.getInstance()).stream()
              .filter(name -> name.contains(args[1]))
              .collect(Collectors.toList());
        } else if (args.length == 3) {
          return Stream.of("potion", "particle", "sound", "command")
              .filter(type -> type.contains(args[2]))
              .collect(Collectors.toList());
        } else if (args.length == 4) {
          switch (args[2]) {
            case "potion":
              return Utils.POTION_NAMES.stream()
                  .filter(name -> name.contains(args[3]))
                  .collect(Collectors.toList());
            case "particle":
              return Utils.PARTICLE_NAMES.stream()
                  .filter(name -> name.contains(args[3]))
                  .collect(Collectors.toList());
            case "sound":
              return Utils.SOUND_NAMES.stream()
                  .filter(name -> name.contains(args[3]))
                  .collect(Collectors.toList());
          }
        }
        break;
    }

    return new ArrayList<>();
  }

}
