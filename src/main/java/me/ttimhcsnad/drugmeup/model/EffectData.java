package me.ttimhcsnad.drugmeup.model;

import java.util.Objects;
import me.ttimhcsnad.drugmeup.model.effects.Effect;

/**
 * The EffectData object used when gathering and applying {@link Drug}s Represents the abstract data
 * model used for easily gathering {@link Drug} effects and applying them at specific times.
 */
public class EffectData {

  private final Effect effect;
  private final DrugUser user;
  private final Drug drug;
  private final String group;
  private final int duration;

  /**
   * Create an EffectData to be used for gathering/applying {@link Drug} effects
   *
   * @param effect The {@link Effect} being stored
   * @param user The {@link DrugUser} being stored
   * @param drug The {@link Drug} being stored
   * @param group The group name of the effect being stored
   * @param duration The effect duration being stored
   */
  public EffectData(Effect effect, DrugUser user, Drug drug, String group, int duration) {
    this.effect = effect;
    this.user = user;
    this.drug = drug;
    this.group = group;
    this.duration = duration;
  }

  /**
   * Create an EffectData to be used for gathering/applying {@link Drug} effects
   *
   * @param effect The {@link Effect} being stored
   * @param data The {@link EffectData} being used as a template to this one
   */
  public EffectData(Effect effect, EffectData data) {
    this(effect, data.getUser(), data.getDrug(), data.getGroup(), data.getDuration());
  }

  /**
   * Retrieve the {@link Effect} stored in this data
   *
   * @return The {@link Effect} stored in this data
   */
  public Effect getEffect() {
    return effect;
  }

  /**
   * Retrieve the {@link DrugUser} stored in this data
   *
   * @return The {@link DrugUser} stored in this data
   */
  public DrugUser getUser() {
    return user;
  }

  /**
   * Retrieve the {@link Drug} stored in this data
   *
   * @return The {@link Drug} stored in this data
   */
  public Drug getDrug() {
    return drug;
  }

  /**
   * Retrieve the group name stored in this data
   *
   * @return The group name stored in this data
   */
  public String getGroup() {
    return this.group;
  }

  /**
   * Retrieve the effect duration stored in this data
   *
   * @return The effect duration stored in this data
   */
  public int getDuration() {
    return duration;
  }

  /**
   * Override equals to abide by value-object semantics Equality checking only additionally verifies
   * the {@link Effect} is equal
   *
   * @param o The {@link EffectData} being checked against
   * @return Whether the objects are equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EffectData that = (EffectData) o;
    return Objects.equals(effect, that.effect);
  }

  /**
   * Override hashCode to abide by value-object semantics
   *
   * @return The hashCode of the {@link EffectData}
   */
  @Override
  public int hashCode() {
    return Objects.hash(effect);
  }
}
