package me.ttimhcsnad.drugmeup.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.enums.TriggerType;
import org.bukkit.ChatColor;
import org.bukkit.Material;

/**
 * A loaded Drug object, contains information about the drug loaded from it's configuration file. A
 * drug follows proper value-object semantics, and assumes immutability.
 */
public class Drug {

  /* Define constants for default values */
  public final static String DEFAULT_NAME = "&fUNDEFINED";
  public final static String DEFAULT_ITEM = "AIR";
  public final static short DEFAULT_DURABILITY = 0;
  public final static String DEFAULT_TRIGGER = TriggerType.USE.name();
  public final static List<String> DEFAULT_MESSAGES = new ArrayList<>(
      Collections.singletonList("&eYou have used @drug"));
  public final static List<String> DEFAULT_LORE = new ArrayList<>(Collections.singletonList(""));
  public final static List<String> DEFAULT_GROUPS = new ArrayList<>(
      Collections.singletonList("default"));
  public final static int DEFAULT_DURATION_MAX = 20;
  public final static int DEFAULT_DURATION_MIN = 5;
  public final static int DEFAULT_COOLDOWN = 0;
  public final static double DEFAULT_OD_CHANCE = 0.1D;
  public final static double DEFAULT_PUKE_CHANCE = 0.1D;
  public final static double DEFAULT_HEARTATTACK_CHANCE = 0.1D;
  public final static double DEFAULT_FIRE_CHANCE = 0.1D;
  public final static double DEFAULT_GROUP_CHANCE = 1.0D;

  /* Class properties */
  private final String name;
  private final String id;
  private final List<String> useMessages;
  private final List<String> lore;
  private final short durability;
  private final int durationMin;
  private final int durationMax;
  private final int cooldown;
  private final double odChance;
  private final double fireChance;
  private final double pukeChance;
  private final double heartAttackChance;
  private final TriggerType trigger;
  private final List<Effect> effects;
  private final List<String> groups;

  /**
   * Represents a Drug. Drugs are loaded from its respective configuration file on startup.
   *
   * @param name The name of the Drug
   * @param id The namespaced material name of the drug.
   * @param useMessages The List of messages shown when the drug is used.
   * @param lore The List of lore on the drug [ Note: Not currently applicable without additional
   * mod ]
   * @param durability The item durability value of the drug. This is applicable for items that
   * require extra data.
   * @param durationMin The minimum duration of the drug effects (in seconds)
   * @param durationMax The maximum duration of the drug effects (in seconds)
   * @param odChance The maximum overdose buildup chance for the drug
   * @param fireChance The maximum fire buildup chance for the drug
   * @param pukeChance The maximum puke buildup chance for the drug
   * @param heartAttackChance The maximum heart attack chance for the drug
   * @param trigger The string value of TriggerType of the drug
   * @param effects The List of Effects possible for the drug. (See {@link Effect}
   * @param cooldown The cooldown from using this drug
   * @param groupChances The chances that the specified groups will be used
   */
  public Drug(String name, String id, List<String> useMessages, List<String> lore,
      short durability, int durationMin, int durationMax, double odChance, double fireChance,
      double pukeChance, double heartAttackChance, String trigger, List<Effect> effects,
      int cooldown, Map<String, Double> groupChances) {
    this.name = name;
    this.id = id;
    this.useMessages = useMessages;
    this.lore = lore;
    this.durability = durability;
    this.durationMin = durationMin;
    this.durationMax = durationMax;
    this.odChance = odChance;
    this.fireChance = fireChance;
    this.pukeChance = pukeChance;
    this.heartAttackChance = heartAttackChance;
    this.trigger = TriggerType.valueOf(trigger.toUpperCase());
    this.effects = effects;
    this.cooldown = cooldown;
    this.groups = new ArrayList<>();
    this.calculateGroupChances(groupChances);
  }

  /**
   * Retrieve the list of effects for the drug.
   *
   * @return A collection of all possible effects for the drug
   */
  public Collection<Effect> getEffects() {
    return new ArrayList<>(this.effects);
  }

  /**
   * Retrieve the trigger type of the Drug
   *
   * @return The form of activation (trigger type) the drug follows.
   */
  public TriggerType getTrigger() {
    return this.trigger;
  }

  /**
   * Retrieve the {@link Material} type the Drug uses
   *
   * @return The {@link Material} type the Drug uses
   */
  public Material getMaterial() {
    return Material.matchMaterial(this.id.replaceAll("minecraft:", ""));
  }

  /**
   * Retrieve the List of use messages for the Drug
   *
   * @return The List of messages for the activation of the drug
   */
  public List<String> getUseMessages() {
    return this.useMessages;
  }

  /**
   * Retrieve the List of lore strings associated with the Drug. NOTE: Lore is not currently
   * applicable in the base mod
   *
   * @return The List of lore strings associated with the Drug.
   */
  public List<String> getLore() {
    return this.lore.stream().map(Utils::parseMessage).collect(Collectors.toList());
  }

  /**
   * Retrieve a random message from the Drug's List of use messages.
   *
   * @return A random message from the Drug's  List of use messages.
   */
  public String getRandomUseMessage() {
    return this.getUseMessages().get(new Random().nextInt(this.getUseMessages().size()));
  }

  /**
   * Retrieve the durability value set for the drug.
   *
   * @return The {@link org.bukkit.inventory.ItemStack} durability value set for the Drug.
   */
  public short getDurability() {
    return this.durability;
  }

  /**
   * Retrieve the minimum duration the drug will last for (in seconds)
   *
   * @return The minimum duration the drug will last for
   */
  public int getMinDuration() {
    return this.durationMin;
  }

  /**
   * Retrieve the maximum duration the drug will last for (in seconds)
   *
   * @return The maximum duration the drug will last for
   */
  public int getMaxDuration() {
    return this.durationMax;
  }

  /**
   * Retrieve the maximum buildup chance of overdosing on the Drug
   *
   * @return The maximum buildup chance of overdosing on the Drug
   */
  public double getOdChance() {
    return this.odChance;
  }

  /**
   * Retrieve the maximum buildup chance of catching on fire for the Drug
   *
   * @return The maximum buildup chance of catching on fire for the Drug
   */
  public double getFireChance() {
    return this.fireChance;
  }

  /**
   * Retrieve the maximum buildup chance of puking on the Drug
   *
   * @return The maximum buildup chance of puking on the Drug
   */
  public double getPukeChance() {
    return this.pukeChance;
  }

  /**
   * Retrieve the maximum buildup chance of having a heart attack on the Drug
   *
   * @return The maximum buildup chance of having a heart attack on the Drug
   */
  public double getHeartAttackChance() {
    return this.heartAttackChance;
  }

  /**
   * Retrieve the cooldown necessary for taking more drugs
   *
   * @return The cooldown necessary for taking more drugs
   */
  public int getCooldown() {
    return this.cooldown;
  }

  public void calculateGroupChances(Map<String, Double> chances) {
    List<String> items = new ArrayList<>();
    chances.keySet().forEach(group -> {
      int amount = (int) (chances.get(group) * 100);
      for (int x = 0; x < amount; x++) {
        this.groups.add(group);
      }
    });
  }

  public String chooseRandomGroup() {
    return groups.get(Utils.random(groups.size(), 0));
  }

  /**
   * Retrieve the colorized form of the Drug name
   *
   * @return The colorized form of the Drug name
   */
  @Override
  public String toString() {
    return ChatColor.translateAlternateColorCodes('&', name);
  }

  /**
   * Override equality checking to ensure value-object equality semantics
   *
   * @param o The object being checked for equality
   * @return Whether the objects are equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Drug drug = (Drug) o;
    return durability == drug.durability &&
        durationMin == drug.durationMin &&
        durationMax == drug.durationMax &&
        Double.compare(drug.odChance, odChance) == 0 &&
        Double.compare(drug.fireChance, fireChance) == 0 &&
        Double.compare(drug.pukeChance, pukeChance) == 0 &&
        Double.compare(drug.heartAttackChance, heartAttackChance) == 0 &&
        Objects.equals(name, drug.name) &&
        Objects.equals(id, drug.id) &&
        Objects.equals(useMessages, drug.useMessages) &&
        Objects.equals(lore, drug.lore) &&
        trigger == drug.trigger &&
        Objects.equals(effects, drug.effects);
  }

  /**
   * Override hashCode functionality to work with value-object semantics
   *
   * @return The hashcode of the Drug
   */
  @Override
  public int hashCode() {
    return Objects
        .hash(name, id, useMessages, lore, durability, durationMin, durationMax,
            odChance, fireChance, pukeChance, heartAttackChance, trigger, effects);
  }
}
