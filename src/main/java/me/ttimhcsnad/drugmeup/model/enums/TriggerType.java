package me.ttimhcsnad.drugmeup.model.enums;

/**
 * The {@link TriggerType} value of {@link me.ttimhcsnad.drugmeup.model.Drug} objects
 */
public enum TriggerType {
  USE, CONSUME, NONE
}