package me.ttimhcsnad.drugmeup.model.enums;

/**
 * The TriggerTime value of {@link me.ttimhcsnad.drugmeup.model.effects.interfaces.TimedEffect}
 * objects
 */
public enum TriggerTime {
  START, END
}