package me.ttimhcsnad.drugmeup.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;
import me.ttimhcsnad.drugmeup.events.SoberEvent;
import me.ttimhcsnad.drugmeup.model.effects.CommandEffect;
import me.ttimhcsnad.drugmeup.model.effects.PotionEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.LoopableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.TimedEffect;
import me.ttimhcsnad.drugmeup.model.enums.TriggerTime;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Represents a DrugUser or a {@link Player} that has taken drugs
 */
public class DrugUser {

  private final UUID uniqueId;
  private Map<Drug, Long> activeDrugs;                  // Store Drug, Time until comedown
  private Map<Drug, Set<EffectData>> gatheredEffects;   // Store Drug, Set of gathered effects
  private Map<Drug, Set<EffectData>> appliedEffects;    // Store Drug, Set of already applied effects
  private Map<LoopableEffect, Long> effectIntervals;    // Store Effect, Next Effect Interval Time
  private Map<DelayableEffect, Long> delayedEffects;    // Store Effect, Time for delay

  private long nextWearoff = 0L;                        // Next negative "wearoff" time
  private long cooldown = 0L;                           // User specific cooldown time

  private float chanceOfOD = 0F;                        // Current chance of OD
  private float chanceOfHeartAttack = 0F;               // Current chance of Heart Attack
  private float chanceOfPuke = 0F;                      // Current chance of Puking
  private float chanceOfFire = 0F;                      // Current chance of Burning

  /**
   * Initialize a DrugUser A DrugUser keeps track of effect data associated with the Drug's the
   * player has taken. A DrugUser manages their percentages of negative effects due to buildup.
   *
   * @param player The {@link Player} being associated with the DrugUser
   */
  public DrugUser(Player player) {
    this.activeDrugs = new HashMap<>();
    this.gatheredEffects = new HashMap<>();
    this.appliedEffects = new HashMap<>();
    this.effectIntervals = new HashMap<>();
    this.delayedEffects = new HashMap<>();
    this.uniqueId = player.getUniqueId();
  }

  /**
   * Retrieve the DrugUser's UUID This is equal to the UUID of the {@link Player} on initialization
   *
   * @return The Unique ID of the DrugUser
   */
  public UUID getUniqueId() {
    return this.uniqueId;
  }

  /**
   * Run a check to see if the DrugUser should be sober for any {@link Drug}s they have taken
   */
  public void checkSober() {
    for (Drug drug : new ArrayList<>(activeDrugs.keySet())) {
      if (System.currentTimeMillis() > this.getComedownTime(drug)) {
        this.sober(drug);
      }
    }
  }

  /**
   * Apply sobriety for all {@link Drug}s the DrugUser has taken
   */
  public void soberAll() {
    new ArrayList<>(this.activeDrugs.keySet()).forEach(this::sober);
  }

  /**
   * Sober the DrugUser from any {@link Drug}s they have taken This will remove all active {@link
   * org.bukkit.potion.PotionEffect}s from the {@link Player} associated with the DrugUser This will
   * execute any pending {@link TimedEffect}s for the {@link Drug}s deactivation This will call the
   * {@link SoberEvent} to be picked up for later use
   *
   * @param drug The {@link Drug} the DrugUser is being sobered for
   */
  public void sober(Drug drug) {
    // Unset potion effects
    Set<EffectData> allEffects = new HashSet<>();
    allEffects.addAll(this.getAllAppliedEffects());
    allEffects.addAll(this.getAllGatheredEffects());
    allEffects.stream()
        .filter(Objects::nonNull)
        .map(EffectData::getEffect)
        .filter(Objects::nonNull)
        .filter(effect -> effect instanceof PotionEffect)
        .map(effect -> ((PotionEffect) effect).getPotionEffect())
        .filter(Objects::nonNull)
        .forEach(potionEffect -> this.getPlayer().removePotionEffect(potionEffect.getType()));
    // Run all "end" commands
    this.getAllGatheredEffects().stream()
        .filter(Objects::nonNull)
        .filter(effectData -> effectData.getEffect() != null)
        .filter(effectData -> effectData.getEffect() instanceof TimedEffect)
        .filter(effectData -> effectData.getEffect() instanceof CommandEffect)
        .filter(effectData -> ((TimedEffect) effectData.getEffect()).getTriggerTime()
            == TriggerTime.END)
        .forEach(effectData -> effectData.getEffect().apply(effectData));
    // Remove all active drugs
    this.activeDrugs.remove(drug);
    this.gatheredEffects.remove(drug);
    this.appliedEffects.remove(drug);
    // Call SoberEvent to be picked up later
    Bukkit.getServer().getPluginManager().callEvent(
        new SoberEvent(this, drug)
    );
  }

  /**
   * Add all {@link EffectData} gathered when the drug was used. This will associate the gathered
   * effects with the {@link Drug} and establish delayedEffect information (if applicable)
   *
   * @param drug The {@link Drug} the effects were gathered for
   * @param gatheredEffects The Set of {@link EffectData} gathered for the {@link Drug}
   */
  public void addGatheredEffects(Drug drug, Set<EffectData> gatheredEffects) {
    // Add gathered EffectData
    this.gatheredEffects.putIfAbsent(drug, new HashSet<>());
    this.gatheredEffects.get(drug).addAll(gatheredEffects);
    // Set delayedEffects appropriately
    this.gatheredEffects.get(drug).stream()
        .filter(Objects::nonNull)
        .map(EffectData::getEffect)
        .filter(Objects::nonNull)
        .filter(effect -> !(effect instanceof LoopableEffect)
            || !((LoopableEffect) effect).doesLoop())
        .filter(effect -> effect instanceof DelayableEffect)
        .map(effect -> (DelayableEffect) effect)
        .filter(DelayableEffect::isDelayed)
        .forEach(delayedEffect ->
            this.delayedEffects
                .put(delayedEffect, System.currentTimeMillis() + delayedEffect.getDelay())
        );
  }

  /**
   * Add all {@link EffectData} that was applied to the DrugUser Adding to the appliedEffect list
   * allows "once-and-done" effects to be distinguished
   *
   * @param drug The {@link Drug} the {@link EffectData} is being associated with
   * @param effectData The {@link EffectData} being added
   */
  public void addAppliedEffect(Drug drug, EffectData effectData) {
    this.appliedEffects.putIfAbsent(drug, new HashSet<>());
    this.appliedEffects.get(drug).add(effectData);
  }

  /**
   * Retrieve a list of the active {@link Drug}s the DrugUser is under the influence of
   *
   * @return The {@link Stream} of {@link Entry} set of drugs the user has taken
   */
  public Map<Drug, Long> getActiveDrugs() {
    return new HashMap<>(this.activeDrugs);
  }

  /**
   * Helper method to check if the user is on any drugs currently
   *
   * @return Whether the user is under the influence of any drug
   */
  public boolean hasActiveDrugs() {
    return this.getActiveDrugs().size() > 0;
  }

  /**
   * Retrieve the User's "Comedown" or sober time associated with the referenced {@link Drug}
   *
   * @param drug The {@link Drug} being checked for sobriety time
   * @return The epoch timestamp of when they will be sober for the {@link Drug}
   */
  public long getComedownTime(Drug drug) {
    return activeDrugs.get(drug);
  }

  /**
   * Retrieve the User's "Comedown" or sober time associated with all active drugs
   *
   * @return The epoch timestamp of when they will be sober for all active drugs
   */
  public long getTotalComedownTime() {
    return activeDrugs.values().stream().mapToLong(time -> (time - System.currentTimeMillis()))
        .sum();
  }

  /**
   * Update the "Comedown" or sober duration for the specified {@link Drug} The duration set will be
   * added to the users current sober time
   *
   * @param drug The {@link Drug} in which the sobriety time is being updated for
   * @param duration The duration until sobriety (in seconds)
   */
  public void updateComedown(Drug drug, int duration) {
    Long currentComedown = activeDrugs.get(drug);
    if (currentComedown != null) {
      activeDrugs.put(drug, currentComedown + (duration * 1000));
    } else {
      this.setComedown(drug, duration);
    }
  }

  /**
   * Set the "Comedown" or sober duration for the specified {@link Drug} The duration set will be
   * added to the epoch timestamp of the System in milliseconds
   *
   * @param drug The {@link Drug} in which the sobriety time is being set for
   * @param duration The duration until sobriety (in seconds)
   */
  public void setComedown(Drug drug, int duration) {
    activeDrugs.put(drug, System.currentTimeMillis() + (duration * 1000));
  }

  /**
   * Helper method to apply a single change to all negative chances
   *
   * @param change The negative chance delta being applied
   */
  public void setAllNegChances(float change) {
    this.setChanceOfOD((this.chanceOfOD < 0.00F) ? 0F : this.chanceOfOD * change);
    this.setChanceOfFire((this.chanceOfFire < 0.00F) ? 0F : this.chanceOfFire * change);
    this.setChanceOfHeartAttack(
        (this.chanceOfHeartAttack < 0.00F) ? 0F : this.chanceOfHeartAttack * change);
    this.setChanceOfPuke((this.chanceOfPuke < 0.00F) ? 0F : this.chanceOfPuke * change);
  }

  /**
   * Retrieve the {@link Player} associated with the DrugUser
   *
   * @return The {@link Player} associated with the {@link DrugUser}
   */
  public Player getPlayer() {
    return Bukkit.getPlayer(this.uniqueId);
  }

  /**
   * Set the next time for negative chances to multiply down
   *
   * @param wearoff The next time for the negative chances to multiply down
   */
  public void setNextWearoff(long wearoff) {
    this.nextWearoff = wearoff;
  }

  /**
   * Retrieve the time for the negative chances to wear down for the DrugUser
   *
   * @return The epoch timestamp of the next wearoff iteration for negative effects
   */
  public long getNextWearoff() {
    return this.nextWearoff;
  }

  /**
   * Retrieve the epoch timestamp of the next effect iteration for the desired {@link
   * LoopableEffect}
   *
   * @param effect The {@link LoopableEffect} to retrieve the next timestamp for
   * @return The timestamp for the next effect iteration for the desired {@link LoopableEffect}
   */
  public long getNextEffectInterval(LoopableEffect effect) {
    this.effectIntervals.putIfAbsent(effect, 0L);
    return this.effectIntervals.get(effect);
  }

  /**
   * Set the next effect iteration time for the desired {@link LoopableEffect} This will add the
   * provided interval
   *
   * @param effect The {@link LoopableEffect} the iteration is being applied to
   * @param interval The interval of the {@link LoopableEffect} (in milliseconds)
   */
  public void setNextEffectInterval(LoopableEffect effect, int interval) {
    this.effectIntervals.put(effect, System.currentTimeMillis() + interval);
  }

  /**
   * Retrieve the epoch timestamp of the next effect iteration for the desired {@link
   * DelayableEffect}
   *
   * @param effect The {@link DelayableEffect} to retrieve the next timestamp for
   * @return The timestamp for the next effect iteration for the desired {@link DelayableEffect}
   */
  public long getDelayedEffectTime(DelayableEffect effect) {
    this.delayedEffects.putIfAbsent(effect, System.currentTimeMillis() + effect.getDelay());
    return this.delayedEffects.get(effect);
  }

  /**
   * Reset the delayed effect iteration time This sets the iteration time to -1 so it does not go
   * off again
   *
   * @param effect The {@link DelayableEffect} the iteration time is being reset for
   */
  public void resetDelayedEffectTime(DelayableEffect effect) {
    this.delayedEffects.remove(effect);
  }

  /**
   * Retrieve a {@link Stream} of gathered {@link EffectData} for the specified {@link Drug}
   *
   * @param drug The {@link Drug} used to retrieve the gathered {@link EffectData}
   * @return A {@link Stream} of gathered {@link EffectData} for the specified {@link Drug}
   */
  public Set<EffectData> getGatheredEffects(Drug drug) {
    return new HashSet<>(this.gatheredEffects.get(drug));
  }

  /**
   * Retrieve a {@link Stream} of all gathered {@link EffectData} for all active {@link Drug}s
   *
   * @return A {@link Stream} of all gathered {@link EffectData} for all active {@link Drug}s
   */
  public Set<EffectData> getAllGatheredEffects() {
    Set<EffectData> result = new HashSet<>();
    this.gatheredEffects
        .forEach((drug, effectDataSet) -> result.addAll(this.getGatheredEffects(drug)));
    return result;
  }

  /**
   * Remove the association between the provided {@link Drug} and the {@link EffectData} The will
   * remove it as a gathered effect as to not be applied again
   *
   * @param drug The {@link Drug} the referencing the {@link EffectData}
   * @param effectData The {@link EffectData} being disassociated
   */
  public void removeGatheredEffect(Drug drug, EffectData effectData) {
    if (this.gatheredEffects.get(drug) != null) {
      this.gatheredEffects.get(drug).remove(effectData);
    }
  }

  /**
   * Retrieve a {@link Stream} of applied {@link EffectData} for the specified {@link Drug}
   *
   * @param drug The {@link Drug} used to retrieve the applied {@link EffectData}
   * @return A {@link Stream} of applied {@link EffectData} for the specified {@link Drug}
   */
  public Set<EffectData> getAppliedEffect(Drug drug) {
    return this.appliedEffects.get(drug);
  }

  /**
   * Retrieve a {@link Stream} of all applied {@link EffectData} for all active {@link Drug}s
   *
   * @return A {@link Stream} of all applied {@link EffectData} for all active {@link Drug}s
   */
  public Set<EffectData> getAllAppliedEffects() {
    Set<EffectData> result = new HashSet<>();
    this.appliedEffects.forEach((drug, effectDataSet) ->
        result.addAll(this.getAppliedEffect(drug)));
    return result;
  }

  /**
   * Set the current DrugUsers drug use cooldown
   *
   * @param cooldown The cooldown being applied (will add to current time)
   */
  public void setCooldown(long cooldown) {
    this.cooldown = System.currentTimeMillis() + cooldown;
  }

  /**
   * Retrieve the DrugUsers current drug cooldown
   *
   * @return The DrugUser's current drug cooldown
   */
  public long getCooldown() {
    return cooldown;
  }

  /**
   * Retrieve the time left for the cooldown (in seconds)
   *
   * @return The time left for the cooldown (in seconds)
   */
  public String getCooldownTimeLeft() {
    return new DecimalFormat("#.#")
        .format(((this.cooldown - System.currentTimeMillis()) / 1000.0D));
  }

  /**
   * Retrieve the DrugUsers current built-up chance of overdose
   *
   * @return The DrugUsers current built-up chance of overdose
   */
  public float getChanceOfOD() {
    return chanceOfOD;
  }

  /**
   * Retrieve the DrugUsers current built-up chance of a heart attack
   *
   * @return The DrugUsers current built-up chance of a heart attack
   */
  public float getChanceOfHeartAttack() {
    return chanceOfHeartAttack;
  }

  /**
   * Retrieve the DrugUsers current built-up chance of puking
   *
   * @return The DrugUsers current built-up chance of puking
   */
  public float getChanceOfPuke() {
    return chanceOfPuke;
  }

  /**
   * Retrieve the DrugUsers current built-up chance of catching fire
   *
   * @return The DrugUsers current built-up chance of catching fire
   */
  public float getChanceOfFire() {
    return chanceOfFire;
  }

  /**
   * Set the DrugUser's current chance of overdosing
   *
   * @param chanceOfOD The new chance of overdosing
   */
  public void setChanceOfOD(float chanceOfOD) {
    this.chanceOfOD = chanceOfOD;
  }

  /**
   * Set the DrugUser's current chance of a heart attack
   *
   * @param chanceOfHeartAttack The new chance of a heart attack
   */
  public void setChanceOfHeartAttack(float chanceOfHeartAttack) {
    this.chanceOfHeartAttack = chanceOfHeartAttack;
  }

  /**
   * Set the DrugUser's current chance of puking
   *
   * @param chanceOfPuke The new chance of puking
   */
  public void setChanceOfPuke(float chanceOfPuke) {
    this.chanceOfPuke = chanceOfPuke;
  }

  /**
   * Set the DrugUser's current chance of catching fire
   *
   * @param chanceOfFire The new chance of catching fire
   */
  public void setChanceOfFire(float chanceOfFire) {
    this.chanceOfFire = chanceOfFire;
  }

}
