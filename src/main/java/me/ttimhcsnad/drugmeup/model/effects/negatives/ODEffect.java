package me.ttimhcsnad.drugmeup.model.effects.negatives;

import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.events.NegativeEffectEvent;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.NegativeEffect;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ODEffect extends Effect implements NegativeEffect {

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (data.getDrug().getOdChance() > 0.00F) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data);
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    DrugUser user = data.getUser();
    Player player = user.getPlayer();
    player.setHealth(0);
    user.setChanceOfOD(0.0F);
    Bukkit.getServer().getPluginManager()
        .callEvent(new NegativeEffectEvent(user, data.getDrug(), this));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean checkChance(DrugUser user) {
    return (int) (user.getChanceOfOD() * 100) >= Utils.random(101, 1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    return super.equals(obj);
  }

}
