package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.enums.TriggerTime;

/**
 * An {@link Effect} that is applied at a specified {@link TriggerTime}
 */
public interface TimedEffect extends IEffect {

  /**
   * Retrieve the {@link TriggerTime} of the {@link TimedEffect} Used to differentiate when the
   * effect will be applied
   *
   * @return The {@link TriggerTime} of the {@link TimedEffect}
   */
  TriggerTime getTriggerTime();

}
