package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import me.ttimhcsnad.drugmeup.model.effects.Effect;

/**
 * A marker interface for denoting a negative {@link Effect}
 */
public interface NegativeEffect extends IEffect {

}
