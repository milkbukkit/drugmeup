package me.ttimhcsnad.drugmeup.model.effects;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.GroupableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.LoopableEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

/**
 * Represents a Particle Effect. ParticleEffect follows proper value-object semantics, and assumes
 * immutability.
 */
public class ParticleEffect extends Effect implements LoopableEffect, DelayableEffect,
    GroupableEffect {

  /**
   * Enumeration for particle location relative to a {@link Player}
   */
  public enum ParticleLocation {
    FEET, HEAD
  }

  /* Define constants for default values */
  public final static boolean DEFAULT_GLOBAL = true;
  public final static int DEFAULT_INTERVAL = 0;
  public final static int DEFAULT_DELAY = 0;
  public final static ParticleLocation DEFAULT_LOCATION = ParticleLocation.HEAD;
  public final static double DEFAULT_CHANCE = 1.0F;
  public final static int DEFAULT_MAX = 50;
  public final static int DEFAULT_MIN = 25;
  public final static double DEFAULT_OFFSET_X = 0.0;
  public final static double DEFAULT_OFFSET_Y = 0.5;
  public final static double DEFAULT_OFFSET_Z = 0.0;
  public final static double DEFAULT_SPEED = 0.05;
  public final static String DEFAULT_DATA = "";

  /* Class properties */
  private final Particle type;
  private final boolean global;
  private final int interval;
  private final int delay;
  private ParticleLocation location;
  private final int minAmount;
  private final int maxAmount;
  private final double offsetX;
  private final double offsetY;
  private final double offsetZ;
  private final double speed;
  private final String data;
  private final List<String> groups;

  /**
   * Create a ParticleEffect
   *
   * @param type The {@link Particle} type being referenced
   * @param global Whether the particles are visible to everyone or just the applicant
   * @param interval The interval of the particles being emitted
   * @param delay The delay of the effect
   * @param location The string representation of {@link ParticleLocation}
   * @param chance The chance the effect being applied
   * @param minAmount The minimum amount of particles to be applied
   * @param maxAmount The maximum amount of particles to be applied
   * @param offsetX The maximum initial spread amount in the x-direction
   * @param offsetY The maximum initial spread amount in the y-direction
   * @param offsetZ The maximum initial spread amount in the z-direction
   * @param speed The speed value of particles (sometimes referred to as extra)
   * @param data The additional material name data of the particle
   * @param groups The groups this effect belongs to
   */
  public ParticleEffect(Particle type, boolean global, int interval, int delay,
      String location, double chance, int minAmount, int maxAmount,
      double offsetX, double offsetY, double offsetZ, double speed, String data,
      List<String> groups) {
    super(chance);
    this.type = type;
    this.global = global;
    this.interval = interval;
    this.delay = delay;
    this.location = ParticleLocation.valueOf(location.toUpperCase());
    this.minAmount = minAmount;
    this.maxAmount = maxAmount;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.offsetZ = offsetZ;
    this.speed = speed;
    this.data = data.equals("") ? null : data.replaceAll("minecraft:", "");
    this.groups = groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (this.groups.contains(data.getGroup())) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data.getUser(), data.getDrug(), data.getGroup(),
            data.getDuration());
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    Player player = data.getUser().getPlayer();

    int amount = Utils.random(this.maxAmount, this.minAmount);
    Location location = (this.location == ParticleLocation.HEAD)
        ? player.getEyeLocation()
        : player.getLocation();

    try {
      if (global) {
        player.getWorld()
            .spawnParticle(this.type, location, amount, this.offsetX, this.offsetY, this.offsetZ,
                this.speed, new MaterialData(Material.matchMaterial(this.data)));
      } else {
        player.spawnParticle(this.type, location, amount, this.offsetX, this.offsetY, this.offsetZ,
            this.speed, new MaterialData(Material.matchMaterial(this.data)));
      }
    } catch (IllegalArgumentException | NullPointerException ignored) {
      if (global) {
        player.getWorld()
            .spawnParticle(this.type, location, amount, this.offsetX, this.offsetY, this.offsetZ,
                this.speed);
      } else {
        player.spawnParticle(this.type, location, amount, this.offsetX, this.offsetY, this.offsetZ,
            this.speed);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDelayed() {
    return this.getDelay() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getDelay() {
    return this.delay;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean doesLoop() {
    return this.getInterval() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getInterval() {
    return this.interval;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<String> getGroups() {
    return this.groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ParticleEffect effect = (ParticleEffect) o;
    return global == effect.global &&
        interval == effect.interval &&
        minAmount == effect.minAmount &&
        maxAmount == effect.maxAmount &&
        type == effect.type &&
        Double.compare(effect.offsetX, offsetX) == 0 &&
        Double.compare(effect.offsetY, offsetY) == 0 &&
        Double.compare(effect.offsetZ, offsetZ) == 0 &&
        Double.compare(effect.speed, speed) == 0 &&
        effect.data != null && effect.data.equals(data) &&
        location == effect.location;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(type, global, interval, location, minAmount, maxAmount,
        offsetX, offsetY, offsetZ, speed, data);
  }

}
