package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import me.ttimhcsnad.drugmeup.model.effects.Effect;

/**
 * An {@link Effect} that is applied at a specified delay
 */
public interface DelayableEffect extends IEffect {

  /**
   * Check whether the specified {@link DelayableEffect} is actually delayed
   *
   * @return Whether the specified {@link DelayableEffect} is actually delayed
   */
  boolean isDelayed();

  /**
   * Retrieve the specified delay for the {@link DelayableEffect}
   *
   * @return The specified delay for the {@link DelayableEffect}
   */
  int getDelay();

}
