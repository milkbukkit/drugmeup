package me.ttimhcsnad.drugmeup.model.effects.negatives;

import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.events.NegativeEffectEvent;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.NegativeEffect;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PukeEffect extends Effect implements NegativeEffect {

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (data.getDrug().getPukeChance() > 0.00F) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data);
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    DrugUser user = data.getUser();
    Player player = user.getPlayer();
    for (int i = 0; i < 9; i++) {
      ItemStack item = player.getInventory().getItem(i);
      if (item != null) {
        ItemStack singleCopy = item.clone();
        Item droppedItem = player.getWorld()
            .dropItemNaturally(player.getEyeLocation(), singleCopy);
        droppedItem.setPickupDelay(20);
        item.setAmount(0);
      }
    }
    user.setChanceOfPuke(0.0F);
    Bukkit.getServer().getPluginManager()
        .callEvent(new NegativeEffectEvent(user, data.getDrug(), this));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean checkChance(DrugUser user) {
    return (int) (user.getChanceOfPuke() * 100) >= Utils.random(101, 1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    return super.equals(obj);
  }

}
