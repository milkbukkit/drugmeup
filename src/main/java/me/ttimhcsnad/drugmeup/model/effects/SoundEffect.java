package me.ttimhcsnad.drugmeup.model.effects;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.GroupableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.LoopableEffect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Represents a Sound effect. SoundEffect follows proper value-object semantics, and assumes
 * immutability.
 */
public class SoundEffect extends Effect implements LoopableEffect, DelayableEffect,
    GroupableEffect {

  /* Define constants for default values */
  public final static boolean DEFAULT_GLOBAL = true;
  public final static int DEFAULT_INTERVAL = 0;
  public final static int DEFAULT_DELAY = 0;
  public final static double DEFAULT_CHANCE = 1.0F;
  public final static double DEFAULT_VOLUME_MAX = 1.0F;
  public final static double DEFAULT_VOLUME_MIN = 0.5F;
  public final static double DEFAULT_PITCH_MAX = 1.0F;
  public final static double DEFAULT_PITCH_MIN = 0.5F;

  /* Class properties */
  private final Sound type;
  private final boolean global;
  private final int interval;
  private final int delay;
  private final double chance;
  private final double minVolume;
  private final double maxVolume;
  private final double minPitch;
  private final double maxPitch;
  private final List<String> groups;

  /**
   * Create a SoundEffect.
   *
   * @param type The {@link Sound} being referenced
   * @param global Whether the sound is played only to the {@link Player} or in the {@link
   * org.bukkit.World}
   * @param interval The interval of the sound (in ms) (Set to 0 to make it not loop)
   * @param delay The delay of the sound (in ms)
   * @param chance The chance that the effect will be applied
   * @param minVolume The minimum volume of the Sound to be played
   * @param maxVolume The maximum volume of the Sound to be played
   * @param minPitch The minimum pitch of the Sound to be played
   * @param maxPitch The maximum pitch of the Sound to be played
   * @param groups The groups this effect belongs to
   */
  public SoundEffect(Sound type, boolean global, int interval, int delay,
      double chance, double minVolume, double maxVolume, double minPitch, double maxPitch,
      List<String> groups) {
    super(chance);
    this.type = type;
    this.global = global;
    this.interval = interval;
    this.delay = delay;
    this.chance = chance;
    this.minVolume = minVolume;
    this.maxVolume = maxVolume;
    this.minPitch = minPitch;
    this.maxPitch = maxPitch;
    this.groups = groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (this.groups.contains(data.getGroup())) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data.getUser(), data.getDrug(), data.getGroup(),
            data.getDuration());
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    DrugUser user = data.getUser();
    Player player = user.getPlayer();

    float volume =
        (float) Utils.random((int) (this.maxVolume * 100), (int) (this.minVolume * 100)) / 100;
    float pitch =
        (float) Utils.random((int) (this.maxPitch * 100), (int) (this.minPitch * 100)) / 100;
    if (global) {
      player.getWorld().playSound(player.getLocation(), this.type, volume, pitch);
    } else {
      player.playSound(player.getLocation(), this.type, volume, pitch);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDelayed() {
    return this.getDelay() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getDelay() {
    return this.delay;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean doesLoop() {
    return this.getInterval() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getInterval() {
    return this.interval;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<String> getGroups() {
    return this.groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SoundEffect that = (SoundEffect) o;
    return global == that.global &&
        interval == that.interval &&
        Double.compare(that.chance, chance) == 0 &&
        Double.compare(that.minVolume, minVolume) == 0 &&
        Double.compare(that.maxVolume, maxVolume) == 0 &&
        Double.compare(that.minPitch, minPitch) == 0 &&
        Double.compare(that.maxPitch, maxPitch) == 0 &&
        type == that.type;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(type, global, interval, chance, minVolume, maxVolume, minPitch, maxPitch);
  }
}
