package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import me.ttimhcsnad.drugmeup.model.effects.Effect;

/**
 * An {@link Effect} that is applied at a specified interval
 */
public interface LoopableEffect extends IEffect {

  /**
   * Check whether the specified {@link LoopableEffect} will actually loop
   *
   * @return Whether the specified {@link LoopableEffect} will actually loop
   */
  boolean doesLoop();

  /**
   * Retrieve the specified interval for the {@link LoopableEffect}
   *
   * @return The specified interval for the {@link LoopableEffect}
   */
  int getInterval();

}
