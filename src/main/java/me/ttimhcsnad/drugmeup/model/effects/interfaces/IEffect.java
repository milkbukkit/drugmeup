package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.Effect;

public interface IEffect {

  /**
   * Check if the effect will be applied This will run the chance evaluation to determine if the
   * {@link Effect} will be applied later
   *
   * @param data The template {@link EffectData} used to determine the chance of gathering
   * @return The {@link EffectData} used to apply later (or null if did not work)
   */
  EffectData gather(EffectData data);

  /**
   * Apply the provided {@link EffectData} The data knows the target player when gathered
   *
   * @param data The {@link EffectData} being applied
   */
  void apply(EffectData data);

  /**
   * Check the chance of application at a later time This can be used when gathering {@link
   * EffectData} for the {@link Effect}
   *
   * @param user The {@link DrugUser} being used to check chances on
   * @return Whether the chance will be successful
   */
  boolean checkChance(DrugUser user);

  /**
   * Retrieve the chance of this Effect's application
   *
   * @return The chance of the Effect's application
   */
  double getChance();

}
