package me.ttimhcsnad.drugmeup.model.effects;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.GroupableEffect;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

/**
 * Represents a Potion Effect. PotionEffect follows proper value-object semantics, and assumes
 * immutability.
 */
public class PotionEffect extends Effect implements DelayableEffect, GroupableEffect {

  /* Define constants for default values */
  public final static int DEFAULT_STRENGTH_MAX = 5;
  public final static int DEFAULT_STRENGTH_MIN = 0;
  public final static int DEFAULT_DELAY = 0;
  public final static double DEFAULT_CHANCE = 1.0F;
  public final static boolean DEFAULT_VISIBLE = true;
  public final static boolean DEFAULT_STACKABLE = true;

  /* Class properties */
  private org.bukkit.potion.PotionEffect effect;
  private final PotionEffectType type;
  private final int strengthMin;
  private final int strengthMax;
  private final int delay;
  private final boolean visible;
  private final boolean stackable;
  private final List<String> groups;

  /**
   * Create a PotionEffect.
   *
   * @param type The {@link PotionEffectType} being referenced
   * @param strengthMin The minimum strength of the potion effect
   * @param strengthMax The maximum strength of the potion effect
   * @param delay The delay of the potion effect (in ms)
   * @param chance the chance the potion effect will be applied
   * @param visible The visibility of the potion effect particles
   * @param stackable Whether the potion effects are stackable (duration and strength)
   * @param groups The groups this effect belongs to
   */
  public PotionEffect(PotionEffectType type, int strengthMin, int strengthMax,
      int delay, double chance, boolean visible, boolean stackable, List<String> groups) {
    super(chance);
    this.type = type;
    this.strengthMin = strengthMin;
    this.strengthMax = strengthMax;
    this.delay = delay;
    this.visible = visible;
    this.stackable = stackable;
    this.groups = groups;
  }

  /**
   * Retrieve the associated {@link org.bukkit.potion.PotionEffect} from this {@link PotionEffect}
   *
   * @return The associated {@link org.bukkit.potion.PotionEffect} from this {@link PotionEffect}
   */
  public org.bukkit.potion.PotionEffect getPotionEffect() {
    return this.effect;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (this.groups.contains(data.getGroup())) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data.getUser(), data.getDrug(), data.getGroup(),
            data.getDuration());
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    DrugUser user = data.getUser();
    Player player = user.getPlayer();

    // Properties
    int TICK_RATE = 20;
    int totalDuration = data.getDuration();
    int amplifier = Utils.random(this.strengthMax, this.strengthMin);

    // Stack duration & amplifier (if applicable)
    for (org.bukkit.potion.PotionEffect activeEffect : player.getActivePotionEffects()) {
      if (activeEffect.getType().equals(this.type)) {
        if (this.stackable) {
          totalDuration += (activeEffect.getDuration() / TICK_RATE);
          amplifier =
              activeEffect.getAmplifier() < amplifier ? amplifier : activeEffect.getAmplifier();
        }
      }
    }

    // Update other drugs comedown if this drug affects the same potion effect
    // (if necessary)
    final int durationF = totalDuration;
    user.getActiveDrugs().entrySet().forEach(entry -> {
      Drug d = entry.getKey();
      if (!d.equals(data.getDrug())) {
        if (user.getAppliedEffect(d) != null) {
          user.getAppliedEffect(d).stream()
              .filter(Objects::nonNull)
              .map(EffectData::getEffect)
              .filter(Objects::nonNull)
              .filter(effect -> effect instanceof PotionEffect)
              .map(effect -> ((PotionEffect) effect).getPotionEffect())
              .filter(effect -> effect.getType().equals(this.type))
              .forEach(effect -> {
                user.setComedown(data.getDrug(), durationF);
                user.setComedown(d, durationF);
              });
        }
      }
    });

    // Apply potion effect
    org.bukkit.potion.PotionEffect potionEffect = new org.bukkit.potion.PotionEffect(
        this.type, totalDuration * TICK_RATE, amplifier, this.visible, this.visible
    );
    player.addPotionEffect(this.effect = potionEffect, this.stackable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDelayed() {
    return this.getDelay() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getDelay() {
    return this.delay;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<String> getGroups() {
    return this.groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PotionEffect that = (PotionEffect) o;
    return
        strengthMin == that.strengthMin &&
            strengthMax == that.strengthMax &&
            visible == that.visible &&
            stackable == that.stackable &&
            Objects.equals(type, that.type);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(type, strengthMin, strengthMax, visible, stackable);
  }
}
