package me.ttimhcsnad.drugmeup.model.effects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.GroupableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.LoopableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.TimedEffect;
import me.ttimhcsnad.drugmeup.model.enums.CommandMode;
import me.ttimhcsnad.drugmeup.model.enums.TriggerTime;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandException;
import org.bukkit.entity.Player;

/**
 * Represents a Command Effect. CommandEffect follows proper value-object semantics, and assumes
 * immutability.
 */
public class CommandEffect extends Effect implements LoopableEffect, DelayableEffect, TimedEffect,
    GroupableEffect {

  /* Define constants for default values */
  public final static List<String> DEFAULT_COMMANDS = new ArrayList<>(
      Collections.singletonList(""));
  public final static TriggerTime DEFAULT_TRIGGER = TriggerTime.START;
  public final static CommandMode DEFAULT_MODE = CommandMode.RANDOM;
  public final static double DEFAULT_CHANCE = 1.0F;
  public final static int DEFAULT_INTERVAL = 0;
  public final static int DEFAULT_DELAY = 0;

  /* Class properties */
  private final List<String> commands;
  private final List<String> groups;
  private final TriggerTime trigger;
  private final CommandMode mode;
  private final int interval;
  private final int delay;

  /**
   * Create a CommandEffect
   *
   * @param commands The commands to be chosen from
   * @param trigger The string representation of a {@link TriggerTime}
   * @param chance The chance the effect will be applied
   * @param interval The interval of the effect (0 = no repeating)
   * @param delay The delay of the effect (0 = no delay)
   * @param groups The groups this effect belongs to
   * @param mode The command-execution mode
   */
  public CommandEffect(List<String> commands, String trigger, double chance,
      int interval, int delay, List<String> groups, String mode) {
    super(chance);
    this.commands = commands;
    this.trigger = TriggerTime.valueOf(trigger.toUpperCase());
    this.mode = CommandMode.valueOf(mode.toUpperCase());
    this.interval = interval;
    this.delay = delay;
    this.groups = groups;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EffectData gather(EffectData data) {
    if (this.groups.contains(data.getGroup())) {
      if (this.checkChance(data.getUser())) {
        return new EffectData(this, data.getUser(), data.getDrug(), data.getGroup(),
            data.getDuration());
      }
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void apply(EffectData data) {
    Player player = data.getUser().getPlayer();

    List<String> commands = new ArrayList<>();
    switch (this.mode) {
      case RANDOM:
        commands.add(this.commands.get(new Random().nextInt(this.commands.size())));
        break;
      case ALL:
        commands.addAll(this.commands);
        break;
    }

    final Pattern SERVER_PATTERN = Pattern.compile("^(server:).*$", Pattern.CASE_INSENSITIVE);
    final Pattern OP_PATTERN = Pattern.compile("^(op:).*$", Pattern.CASE_INSENSITIVE);

    commands.forEach(command -> {
      command = command.replaceAll("@p", player.getName());
      command = ChatColor.translateAlternateColorCodes('&', command);

      final Matcher SERVER_MATCHER = SERVER_PATTERN.matcher(command);
      final Matcher OP_MATCHER = OP_PATTERN.matcher(command);

      try {
        // Run command as Server
        if (SERVER_MATCHER.matches()) {
          command = command.replaceAll(SERVER_MATCHER.group(1), "");
          Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), command);
        }
        // Force player to run command as OP
        else if (OP_MATCHER.matches()) {
          command = command.replaceAll(OP_MATCHER.group(1), "");
          boolean changedOp = false;
          try {
            if ((!player.isOp())) {
              changedOp = true;
              player.setOp(true);
            }
            Bukkit.getServer().dispatchCommand(player, command);
          } finally {
            if (changedOp) {
              player.setOp(false);
            }
          }
        }
        // Run command normally
        else {
          Bukkit.getServer().dispatchCommand(player, command);
        }
      } catch (CommandException ex) {
        ex.printStackTrace();
      }
    });
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDelayed() {
    return this.getDelay() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getDelay() {
    return this.delay;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean doesLoop() {
    return this.interval != 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getInterval() {
    return this.interval;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TriggerTime getTriggerTime() {
    return this.trigger;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Collection<String> getGroups() {
    return this.groups;
  }

  /**
   * Retrieve the command execution mode for this effect
   *
   * @return The command execution mode for this effect
   */
  public CommandMode getMode() {
    return this.mode;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommandEffect that = (CommandEffect) o;
    return
        interval == that.interval &&
            delay == that.delay &&
            Objects.equals(commands, that.commands) &&
            trigger == that.trigger;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(commands, trigger, interval, delay);
  }
}