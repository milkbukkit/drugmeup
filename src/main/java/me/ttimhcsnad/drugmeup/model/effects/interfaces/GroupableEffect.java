package me.ttimhcsnad.drugmeup.model.effects.interfaces;

import java.util.Collection;
import me.ttimhcsnad.drugmeup.model.effects.Effect;

/**
 * An {@link Effect} that can be grouped up to have different effect group probabilities
 */
public interface GroupableEffect extends IEffect {

  /**
   * Retrieve the groups this {@link Effect} belongs to
   *
   * @return The group names this {@link Effect} has
   */
  Collection<String> getGroups();

}
