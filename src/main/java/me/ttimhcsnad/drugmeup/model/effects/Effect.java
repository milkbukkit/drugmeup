package me.ttimhcsnad.drugmeup.model.effects;

import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.IEffect;

public abstract class Effect implements IEffect {

  private double chance;

  /**
   * Initialize an Effect with the desired properties
   *
   * @param chance The chance of effect application
   */
  public Effect(double chance) {
    this.chance = chance;
  }

  /**
   * Initialize an Effect with the desired properties
   */
  public Effect() {
    this.chance = 0D;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean checkChance(DrugUser user) {
    return (int) (this.chance * 100) >= Utils.random(101, 1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getChance() {
    return this.chance;
  }

}
