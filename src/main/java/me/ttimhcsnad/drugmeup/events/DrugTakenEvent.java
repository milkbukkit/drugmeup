package me.ttimhcsnad.drugmeup.events;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.EffectData;
import me.ttimhcsnad.drugmeup.model.effects.CommandEffect;
import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.effects.ParticleEffect;
import me.ttimhcsnad.drugmeup.model.effects.PotionEffect;
import me.ttimhcsnad.drugmeup.model.effects.SoundEffect;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * A custom {@link Event} to be fired when a {@link Drug} has been taken
 */
public class DrugTakenEvent extends Event {

  private static final HandlerList handlers = new HandlerList();
  private Player player;
  private Drug drug;
  private Collection<EffectData> appliedEffects;

  /**
   * Create a new DrugTakenEvent to be called
   *
   * @param player The {@link Player} involved in the event
   * @param drug The {@link Drug} involved in the event
   * @param appliedEffects A collection of the {@link EffectData} gathered during the event
   */
  public DrugTakenEvent(Player player, Drug drug, Collection<EffectData> appliedEffects) {
    this.player = player;
    this.drug = drug;
    this.appliedEffects = appliedEffects;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  /**
   * Required method to retrieve the handlers for the event
   *
   * @return The {@link HandlerList} for the event
   */
  public static HandlerList getHandlerList() {
    return handlers;
  }

  /**
   * Retrieve the {@link Player} involved in the event
   *
   * @return The {@link Player} involved in the event
   */
  public Player getPlayer() {
    return this.player;
  }

  /**
   * Retrieve the {@link Drug} involved in the event
   *
   * @return The {@link Drug} involved in the event
   */
  public Drug getDrug() {
    return this.drug;
  }

  /**
   * Check if the event was cancelled
   *
   * @return If the event was cancelled
   */
  public boolean isCancelled() {
    return this.appliedEffects == null;
  }

  /**
   * Retrieve a collection of all applied {@link Effect}s from the event
   *
   * @return A collection of all applied {@link Effect}s from the event
   */
  public Collection<Effect> getAppliedEffects() {
    return this.appliedEffects.stream()
        .filter(Objects::nonNull)
        .filter(effectData -> effectData.getEffect() != null)
        .map(EffectData::getEffect)
        .collect(Collectors.toList());
  }

  /**
   * Retrieve a collection of all applied {@link PotionEffect}s from the event
   *
   * @return A collection of all applied {@link PotionEffect}s from the event
   */
  public Collection<PotionEffect> getAppliedPotionEffects() {
    return this.getAppliedEffects().stream()
        .filter(effect -> effect instanceof PotionEffect)
        .map(effect -> (PotionEffect) effect)
        .collect(Collectors.toList());
  }

  /**
   * Retrieve a collection of all applied {@link ParticleEffect}s from the event
   *
   * @return A collection of all applied {@link ParticleEffect}s from the event
   */
  public Collection<ParticleEffect> getAppliedParticleEffects() {
    return this.getAppliedEffects().stream()
        .filter(effect -> effect instanceof ParticleEffect)
        .map(effect -> (ParticleEffect) effect)
        .collect(Collectors.toList());
  }

  /**
   * Retrieve a collection of all applied {@link SoundEffect}s from the event
   *
   * @return A collection of all applied {@link SoundEffect}s from the event
   */
  public Collection<SoundEffect> getAppliedSoundEffects() {
    return this.getAppliedEffects().stream()
        .filter(effect -> effect instanceof SoundEffect)
        .map(effect -> (SoundEffect) effect)
        .collect(Collectors.toList());
  }

  /**
   * Retrieve a collection of all applied {@link CommandEffect}s from the event
   *
   * @return A collection of all applied {@link CommandEffect}s from the event
   */
  public Collection<CommandEffect> getAppliedCommandEffects() {
    return this.getAppliedEffects().stream()
        .filter(effect -> effect instanceof CommandEffect)
        .map(effect -> (CommandEffect) effect)
        .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DrugTakenEvent that = (DrugTakenEvent) o;
    return Objects.equals(player, that.player) &&
        Objects.equals(drug, that.drug) &&
        Objects.equals(appliedEffects, that.appliedEffects);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(player, drug, appliedEffects);
  }
}
