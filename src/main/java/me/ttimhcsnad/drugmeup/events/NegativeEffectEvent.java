package me.ttimhcsnad.drugmeup.events;

import java.util.Objects;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.NegativeEffect;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * A custom {@link Event} to be fired when a {@link NegativeEffect} has been activated
 */
public class NegativeEffectEvent extends Event {

  private static final HandlerList handlers = new HandlerList();
  private final DrugUser drugUser;
  private final Drug drug;
  private final NegativeEffect effect;

  /**
   * Create a new NegativeEffectEvent to be called
   *
   * @param drugUser The {@link DrugUser} involved in the event
   * @param drug The {@link Drug} involved in the event
   * @param effect The {@link NegativeEffect} involved in the event
   */
  public NegativeEffectEvent(DrugUser drugUser, Drug drug, NegativeEffect effect) {
    this.drugUser = drugUser;
    this.drug = drug;
    this.effect = effect;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  /**
   * Required method to retrieve the handlers for the event
   *
   * @return The {@link HandlerList} for the event
   */
  public static HandlerList getHandlerList() {
    return handlers;
  }

  /**
   * Retrieve the {@link DrugUser} involved in the event
   *
   * @return The {@link DrugUser} involved in the event
   */
  public DrugUser getDrugUser() {
    return this.drugUser;
  }

  /**
   * Retrieve the {@link Drug} involved in the event
   *
   * @return The {@link Drug} involved in the event
   */
  public Drug getDrug() {
    return this.drug;
  }

  /**
   * Retrieve the {@link NegativeEffect} involved in the event
   *
   * @return The {@link NegativeEffect} involved in the event
   */
  public NegativeEffect getEffect() {
    return this.effect;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NegativeEffectEvent that = (NegativeEffectEvent) o;
    return Objects.equals(drugUser, that.drugUser) &&
        Objects.equals(drug, that.drug) &&
        Objects.equals(effect, that.effect);
  }

  @Override
  public int hashCode() {
    return Objects.hash(drugUser, drug, effect);
  }
}
