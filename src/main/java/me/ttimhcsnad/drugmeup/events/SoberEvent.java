package me.ttimhcsnad.drugmeup.events;

import java.util.Objects;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * A custom {@link Event} to be fired when a {@link DrugUser} has sobered from a {@link Drug}
 */
public class SoberEvent extends Event {

  private static final HandlerList handlers = new HandlerList();
  private final DrugUser user;
  private final Drug drug;

  /**
   * Create a new SoberEvent to be called
   *
   * @param user The {@link DrugUser} involved in the event
   * @param drug The {@link Drug} involved in the event
   */
  public SoberEvent(DrugUser user, Drug drug) {
    this.user = user;
    this.drug = drug;
  }

  /**
   * {@inheritDoc} =
   */
  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  /**
   * Required method to retrieve the handlers for the event
   *
   * @return The {@link HandlerList} for the event
   */
  public static HandlerList getHandlerList() {
    return handlers;
  }

  /**
   * Retrieve the {@link DrugUser} involved in the event
   *
   * @return The {@link DrugUser} involved in the event
   */
  public DrugUser getUser() {
    return this.user;
  }

  /**
   * Retrieve the {@link Drug} involved in the event
   *
   * @return The {@link Drug} involved in the event
   */
  public Drug getDrug() {
    return this.drug;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SoberEvent that = (SoberEvent) o;
    return Objects.equals(user, that.user) &&
        Objects.equals(drug, that.drug);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(user, drug);
  }
}
