package me.ttimhcsnad.drugmeup;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffectType;

/**
 * A collection of static utility methods used throughout the plugin
 */
public class Utils {

  /**
   * A list of all valid {@link Material} names in {@link org.bukkit.Bukkit} The list is appended
   * with "minecraft:" for each material type and forced lowercase
   */
  public static Collection<String> MATERIAL_NAMES =
      Arrays.stream(Material.values())
          .map(mat -> "minecraft:" + mat.name().toLowerCase())
          .collect(Collectors.toList());

  /**
   * A list of all valid {@link PotionEffectType} names in {@link org.bukkit.Bukkit} The list is
   * appended with "minecraft:" for each material type and forced lowercase
   */
  public static Collection<String> POTION_NAMES =
      Arrays.stream(PotionEffectType.values())
          .filter(Objects::nonNull)
          .map(potType -> "minecraft:" + potType.getName().toLowerCase())
          .collect(Collectors.toList());

  /**
   * A list of all valid {@link Particle} names in {@link org.bukkit.Bukkit} The list is appended
   * with "minecraft:" for each material type and forced lowercase
   */
  public static Collection<String> PARTICLE_NAMES =
      Arrays.stream(Particle.values())
          .map(particle -> "minecraft:" + particle.name().toLowerCase())
          .collect(Collectors.toList());

  /**
   * A list of all valid {@link Sound} names in {@link org.bukkit.Bukkit} The list is appended with
   * "minecraft:" for each material type and forced lowercase
   */
  public static Collection<String> SOUND_NAMES =
      Arrays.stream(Sound.values())
          .map(sound -> "minecraft:" + sound.name().toLowerCase())
          .collect(Collectors.toList());

  /**
   * Helper method to parse a message into a colorized version of itself
   *
   * @param message The message being parsed
   * @return The parsed message
   */
  public static String parseMessage(String message) {
    return ChatColor.translateAlternateColorCodes('&', message);
  }

  /**
   * Helper method to parse a message into a colorized version of itself Replaces any instance of
   * "@p" with the provided player's name
   *
   * @param message The message being parsed
   * @param player The player whose name is being parsed
   * @return The parsed message
   */
  public static String parseMessage(String message, Player player) {
    return parseMessage(message.replaceAll("@p", player.getDisplayName()));
  }

  /**
   * Helper method to parse a message into a colorized version of itself Replaces any instance of
   * "@time" with the provided users cooldown time
   *
   * @param message The message being parsed
   * @param user The DrugUser whose cooldown  time is being parsed
   * @return The parsed message
   */
  public static String parseMessage(String message, DrugUser user) {
    return parseMessage(message.replaceAll("@time", user.getCooldownTimeLeft()), user.getPlayer());
  }

  /**
   * Helper method to parse a message into a colorized version of itself Replaces any instance of
   * "@p" with the provided player's name Replaces any instance of "@drug" with the provided drug's
   * name
   *
   * @param message The message being parsed
   * @param player The player whose name is being parsed
   * @param drug The drug whose name is being parsed
   * @return The parsed message
   */
  public static String parseMessage(String message, Player player, Drug drug) {
    return parseMessage(message.replaceAll("@drug", drug.toString()), player);
  }

  /**
   * Helper method to parse a message into a colorized version of itself Replaces any instance of
   * "@p" with the provided playerArg value
   *
   * @param message The message being parsed
   * @param playerArg The player whose name is being parsed
   * @return The parsed message
   */
  public static String parseMessage(String message, String playerArg) {
    return parseMessage(message.replaceAll("@p", playerArg));
  }

  /**
   * Helper method to provide a message alerting the provided player they do not have required
   * permissions.
   *
   * @param player The player being sent the message
   */
  public static void noPermission(Player player) {
    player.sendMessage(parseMessage("&c[DrugMeUp] Sorry you don't have permission to do that!"));
  }

  /**
   * Helper method to provide a message alerting the provided {@link CommandSender} they do not have
   * required permissions.
   *
   * @param sender The {@link CommandSender} being sent the message
   */
  public static void noPermission(CommandSender sender) {
    if (sender instanceof Player) {
      Utils.noPermission((Player) sender);
    } else {
      sender.sendMessage(parseMessage("[DrugMeUp] Sorry you don't have permission to do that!"));
    }
  }

  /**
   * Helper method to provide a message alerting the provided {@link CommandSender} that the desired
   * player is invalid.
   *
   * @param sender The {@link CommandSender} performing the requested command
   * @param playerName The attempted player's name
   */
  public static void invalidPlayer(CommandSender sender, String playerName) {
    sender.sendMessage(parseMessage("&c[DrugMeUp] &a@p &eis not online!", playerName));
  }

  /**
   * Provides a random integer value between the provided bounds If the difference between the upper
   * and lower is less than or equal to 0 it will return the lower bounds This uses Java's {@link
   * Random} generation - Lower bounds are inclusive - Upper bounds are exclusive
   *
   * @param upper The upper bounds of the randomization
   * @param lower The lower bounds of the randomization
   * @return The randomized value
   */
  public static int random(int upper, int lower) {
    Random rand = new Random();
    if (upper - lower > 0) {
      return rand.nextInt((upper - lower)) + lower;
    } else {
      return lower;
    }
  }

  /**
   * Joins an array of objects with the provided separator at the desired startIndex and endIndex
   *
   * @param array The array being joined from
   * @param separator The separator token being checked for
   * @param startIndex The index at which to concatenate
   * @param endIndex The last index at which to stop concatenation
   * @return The joined string
   */
  public static String join(Object[] array, String separator, int startIndex, int endIndex) {
    if (array == null) {
      return null;
    }
    if (separator == null) {
      separator = "";
    }
    StringBuilder sb = new StringBuilder();
    for (int i = startIndex; i < endIndex; i++) {
      if (i > startIndex) {
        sb.append(separator);
      }
      if (array[i] != null) {
        sb.append(array[i]);
      }
    }
    return sb.toString();
  }

  /**
   * Helper function to retrieve a list of current {@link Drug} configuration file names. Can be
   * useful when checking for drugs that are not loaded into memory yet, but have a file.
   *
   * @param plugin The provided {@link DrugMeUp} instance used to reference the plugin's data
   * folder
   * @return The collection of {@link Drug} file names
   */
  public static Collection<String> getDrugFileNames(Plugin plugin) {
    List<String> fileNames = new ArrayList<>();
    try {
      fileNames = Files
          .list(FileSystems.getDefault().getPath(plugin.getDataFolder() + "/drugs/"))
          .map(path -> path.getFileName().toString().replaceAll(".yml", ""))
          .collect(Collectors.toList());
    } catch (IOException ignored) {
    }
    return fileNames;
  }

  /**
   * Check if the provided key in the provided {@link ConfigurationSection} is valid and existent
   *
   * @param config The provided configuration section being checked
   * @param key The key being checked for
   * @param defaultVal The default value if the value is not retrieved properly
   * @return The resulting value. This will default to the provided default value if it could not
   * load from the key. If the default value is loaded, this method sets the section to have the key
   * with the default value.
   * @throws Exception When a key is not correctly set.
   */
  public static Object checkKey(ConfigurationSection config, String key, Object defaultVal)
      throws Exception {
    return checkKey(config, key, defaultVal, true);
  }

  /**
   * Check if the provided key in the provided {@link ConfigurationSection} is valid and existent
   *
   * @param config The provided configuration section being checked
   * @param key The key being checked for
   * @param defaultVal The default value if the value is not retrieved properly
   * @param force This will determine if the default value is saved if the key was not found
   * @return The resulting value. This will default to the provided default value if it could not
   * load from the key. If the default value is loaded, this method sets the section to have the key
   * with the default value. (Only if the force param is set to true)
   * @throws Exception When a key is not correctly set.
   */
  public static Object checkKey(ConfigurationSection config, String key, Object defaultVal,
      boolean force)
      throws Exception {
    Object value = config.get(key);
    if (value != null) {
      if (value.getClass().equals(defaultVal.getClass())) {
        return value;
      } else {
        throw new Exception(
            "Error parsing config, invalid option: " + config.getCurrentPath() + "." + key);
      }
    }
    if (force) {
      config.getRoot().set(config.getCurrentPath() + "." + key, defaultVal);
    }
    return defaultVal;
  }

  /**
   * Check if a config section is present, if not, create it
   *
   * @param section The {@link ConfigurationSection} being checked
   * @param name The name of the section checking for
   */
  public static void checkSection(ConfigurationSection section, String name) {
    checkSection(section, name, new HashMap<>());
  }

  /**
   * Check if a config section is present, if not, create it
   *
   * @param section The {@link ConfigurationSection} being checked
   * @param name The name of the section checking for
   * @param defaults The default objects that should be loaded into the section
   */
  public static void checkSection(ConfigurationSection section, String name, Map<?, ?> defaults) {
    if (section.getConfigurationSection(name) == null) {
      section.createSection(name, defaults);
    }
  }

  /**
   * Helper method to log to console with chat colors
   *
   * @param plugin The instance of {@link DrugMeUp}
   * @param msg The message being logged
   */
  public static void logColor(DrugMeUp plugin, String msg) {
    String prefix = "[" + plugin.getName() + "] ";
    msg = parseMessage(msg);
    plugin.getServer().getConsoleSender().sendMessage(prefix + msg);
  }

}
