package me.ttimhcsnad.drugmeup.managers.factories;

import static me.ttimhcsnad.drugmeup.Utils.checkKey;
import static me.ttimhcsnad.drugmeup.Utils.checkSection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.effects.Effect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.CatchFireEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.HeartAttackEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.ODEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.PukeEffect;
import org.bukkit.configuration.ConfigurationSection;

public class DrugFactory {

  /**
   * Factory method for loading a {@link Drug} from the specified {@link ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link Drug}
   * @return The resulting {@link Drug} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static Drug loadDrug(ConfigurationSection config) throws Exception {
    String name = (String) checkKey(config, "name", Drug.DEFAULT_NAME);
    String id = (String) checkKey(config, "item", Drug.DEFAULT_ITEM);
    return loadDrug(config, name, id);
  }

  /**
   * Factory method for loading a {@link Drug} from the specified {@link ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link Drug}
   * @param name The pre-specified name for the {@link Drug}
   * @param id The pre-specified item name for the {@link Drug}
   * @return The resulting {@link Drug} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static Drug loadDrug(ConfigurationSection config, String name, String id)
      throws Exception {
    checkKey(config, "name", name);
    checkKey(config, "item", id);

    // Trigger
    String trigger = (String) checkKey(config, "trigger", Drug.DEFAULT_TRIGGER);

    // Messages
    @SuppressWarnings("unchecked") List<String> messages = (List<String>) checkKey(config,
        "messages", Drug.DEFAULT_MESSAGES);

    // (OPTIONAL) Lore
    @SuppressWarnings("unchecked") List<String> lore = (List<String>) checkKey(config, "lore",
        Drug.DEFAULT_LORE, false);

    // Check durability specification
    String[] split = id.split(Pattern.quote("."));
    short durability = Drug.DEFAULT_DURABILITY;
    if (split.length > 1) {
      id = split[0];
      durability = Short.parseShort(split[1]);
    }

    // Use with others
    int cooldown = (int) checkKey(config, "cooldown", Drug.DEFAULT_COOLDOWN);

    // Duration
    int durationMin = (int) checkKey(config, "duration.min", Drug.DEFAULT_DURATION_MIN);
    int durationMax = (int) checkKey(config, "duration.max", Drug.DEFAULT_DURATION_MAX);

    // Negatives
    double odChance = (double) checkKey(config, "negatives.overdose", Drug.DEFAULT_OD_CHANCE);
    double pukeChance = (double) checkKey(config, "negatives.puke", Drug.DEFAULT_PUKE_CHANCE);
    double fireChance = (double) checkKey(config, "negatives.fire", Drug.DEFAULT_FIRE_CHANCE);
    double heartAttackChance = (double) checkKey(config, "negatives.heartattack",
        Drug.DEFAULT_HEARTATTACK_CHANCE);

    // Group Chances
    Map<String, Double> defaultGroupChances = new HashMap<>();
    defaultGroupChances.put(Drug.DEFAULT_GROUPS.get(0), Drug.DEFAULT_GROUP_CHANCE);
    checkSection(config, "groups", defaultGroupChances);
    Map<String, Double> groupChances = new HashMap<>();
    for (String key : config.getConfigurationSection("groups").getKeys(false)) {
      groupChances.put(key, (Double) checkKey(config, "groups." + key, Drug.DEFAULT_GROUP_CHANCE));
    }

    // Effects
    List<Effect> effects = new ArrayList<>();

    checkSection(config, "effects");
    checkSection(config, "effects.potions");
    checkSection(config, "effects.particles");
    checkSection(config, "effects.sounds");
    checkSection(config, "effects.commands");

    for (String key : config.getConfigurationSection("effects.potions").getKeys(false)) {
      effects.add(EffectFactory
          .loadPotionEffect(config.getConfigurationSection("effects.potions." + key)));
    }
    for (String key : config.getConfigurationSection("effects.particles").getKeys(false)) {
      effects.add(EffectFactory
          .loadParticleEffect(config.getConfigurationSection("effects.particles." + key)));
    }
    for (String key : config.getConfigurationSection("effects.sounds").getKeys(false)) {
      effects.add(
          EffectFactory.loadSoundEffect(config.getConfigurationSection("effects.sounds." + key)));
    }
    for (String key : config.getConfigurationSection("effects.commands").getKeys(false)) {
      effects.add(EffectFactory
          .loadCommandEffect(config.getConfigurationSection("effects.commands." + key)));
    }

    // Load negative effects
    effects.add(new ODEffect());
    effects.add(new PukeEffect());
    effects.add(new HeartAttackEffect());
    effects.add(new CatchFireEffect());

    return new Drug(name, id, messages, lore, durability, durationMin, durationMax,
        odChance, fireChance, pukeChance, heartAttackChance, trigger, effects, cooldown,
        groupChances);
  }

}
