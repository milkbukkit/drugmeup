package me.ttimhcsnad.drugmeup.managers.factories;

import static me.ttimhcsnad.drugmeup.Utils.checkKey;
import static me.ttimhcsnad.drugmeup.Utils.checkSection;

import java.util.ArrayList;
import java.util.List;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.effects.CommandEffect;
import me.ttimhcsnad.drugmeup.model.effects.ParticleEffect;
import me.ttimhcsnad.drugmeup.model.effects.PotionEffect;
import me.ttimhcsnad.drugmeup.model.effects.SoundEffect;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionEffectType;

public class EffectFactory {

  /**
   * Factory method for loading a {@link PotionEffect} from the specified {@link
   * ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link PotionEffect}
   * @return The resulting {@link PotionEffect} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static PotionEffect loadPotionEffect(ConfigurationSection config) throws Exception {
    String typeName = config.getName().toLowerCase();
    if (Utils.POTION_NAMES.contains(typeName)) {
      PotionEffectType type = PotionEffectType
          .getByName(typeName.replaceAll("minecraft:", "").toUpperCase());
      @SuppressWarnings("unchecked") List<String> groups = (List<String>) checkKey(config, "groups",
          new ArrayList<>(Drug.DEFAULT_GROUPS));
      checkSection(config, "strength");
      int strengthMin = (int) checkKey(config, "strength.min", PotionEffect.DEFAULT_STRENGTH_MIN);
      int strengthMax = (int) checkKey(config, "strength.max", PotionEffect.DEFAULT_STRENGTH_MAX);
      int delay = (int) checkKey(config, "delay", PotionEffect.DEFAULT_DELAY);
      double chance = (double) checkKey(config, "chance", PotionEffect.DEFAULT_CHANCE);
      boolean visible = (boolean) checkKey(config, "visible", PotionEffect.DEFAULT_VISIBLE);
      boolean stackable = (boolean) checkKey(config, "stackable", PotionEffect.DEFAULT_STACKABLE);
      return new PotionEffect(type, strengthMin, strengthMax, delay, chance, visible, stackable,
          groups);
    } else {
      throw new Exception("Invalid potion effect name: " + config.getName());
    }
  }

  /**
   * Factory method for loading a {@link ParticleEffect} from the specified {@link
   * ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link ParticleEffect}
   * @return The resulting {@link ParticleEffect} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static ParticleEffect loadParticleEffect(ConfigurationSection config) throws Exception {
    String typeName = config.getName().toLowerCase();
    if (Utils.PARTICLE_NAMES.contains(typeName)) {
      Particle type = Particle.valueOf(typeName.replaceAll("minecraft:", "").toUpperCase());
      @SuppressWarnings("unchecked") List<String> groups = (List<String>) checkKey(config, "groups",
          new ArrayList<>(Drug.DEFAULT_GROUPS));
      boolean global = (boolean) checkKey(config, "global", ParticleEffect.DEFAULT_GLOBAL);
      int interval = (int) checkKey(config, "interval", ParticleEffect.DEFAULT_INTERVAL);
      int delay = (int) checkKey(config, "delay", ParticleEffect.DEFAULT_DELAY);
      String location = (String) checkKey(config, "location",
          ParticleEffect.DEFAULT_LOCATION.name());
      double chance = (double) checkKey(config, "chance", ParticleEffect.DEFAULT_CHANCE);
      checkSection(config, "amount");
      int minAmount = (int) checkKey(config, "amount.min", ParticleEffect.DEFAULT_MIN);
      int maxAmount = (int) checkKey(config, "amount.max", ParticleEffect.DEFAULT_MAX);
      checkSection(config, "settings");
      double offsetX = (double) checkKey(config, "settings.x-offset",
          ParticleEffect.DEFAULT_OFFSET_X);
      double offsetY = (double) checkKey(config, "settings.y-offset",
          ParticleEffect.DEFAULT_OFFSET_Y);
      double offsetZ = (double) checkKey(config, "settings.z-offset",
          ParticleEffect.DEFAULT_OFFSET_Z);
      double speed = (double) checkKey(config, "settings.speed", ParticleEffect.DEFAULT_SPEED);
      String data = (String) checkKey(config, "settings.data", ParticleEffect.DEFAULT_DATA);
      return new ParticleEffect(type, global, interval, delay, location, chance, minAmount,
          maxAmount, offsetX, offsetY, offsetZ, speed, data, groups);
    } else {
      throw new Exception("Invalid particle effect name: " + config.getName());
    }
  }

  /**
   * Factory method for loading a {@link SoundEffect} from the specified {@link
   * ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link SoundEffect}
   * @return The resulting {@link SoundEffect} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static SoundEffect loadSoundEffect(ConfigurationSection config) throws Exception {
    String typeName = config.getName().toLowerCase();
    if (Utils.SOUND_NAMES.contains(typeName)) {
      Sound type = Sound.valueOf(typeName.replaceAll("minecraft:", "").toUpperCase());
      @SuppressWarnings("unchecked") List<String> groups = (List<String>) checkKey(config, "groups",
          new ArrayList<>(Drug.DEFAULT_GROUPS));
      boolean global = (boolean) checkKey(config, "global", SoundEffect.DEFAULT_GLOBAL);
      int interval = (int) checkKey(config, "interval", SoundEffect.DEFAULT_INTERVAL);
      int delay = (int) checkKey(config, "delay", SoundEffect.DEFAULT_DELAY);
      double chance = (double) checkKey(config, "chance", SoundEffect.DEFAULT_CHANCE);
      checkSection(config, "volume");
      double minVolume = (double) checkKey(config, "volume.min", SoundEffect.DEFAULT_VOLUME_MIN);
      double maxVolume = (double) checkKey(config, "volume.max", SoundEffect.DEFAULT_VOLUME_MAX);
      checkSection(config, "pitch");
      double minPitch = (double) checkKey(config, "pitch.min", SoundEffect.DEFAULT_PITCH_MIN);
      double maxPitch = (double) checkKey(config, "pitch.max", SoundEffect.DEFAULT_PITCH_MAX);
      return new SoundEffect(type, global, interval, delay, chance, minVolume, maxVolume, minPitch,
          maxPitch, groups);
    } else {
      throw new Exception("Invalid sound effect name: " + config.getName());
    }
  }

  /**
   * Factory method for loading a {@link CommandEffect} from the specified {@link
   * ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link CommandEffect}
   * @return The resulting {@link CommandEffect} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static CommandEffect loadCommandEffect(ConfigurationSection config) throws Exception {
    @SuppressWarnings("unchecked") List<String> commands = (List<String>) checkKey(config,
        "commands", CommandEffect.DEFAULT_COMMANDS);
    return loadCommandEffect(config, commands);
  }

  /**
   * Factory method for loading a {@link CommandEffect} from the specified {@link
   * ConfigurationSection}
   *
   * @param config The {@link ConfigurationSection} being used to load the {@link CommandEffect}
   * @param commands The list of commands being loaded
   * @return The resulting {@link CommandEffect} loaded from the {@link ConfigurationSection}
   * @throws Exception When config entries are invalid
   */
  public static CommandEffect loadCommandEffect(ConfigurationSection config, List<String> commands)
      throws Exception {
    @SuppressWarnings("unchecked") List<String> groups = (List<String>) checkKey(config, "groups",
        new ArrayList<>(Drug.DEFAULT_GROUPS));
    String trigger = (String) checkKey(config, "trigger", CommandEffect.DEFAULT_TRIGGER.name());
    String mode = (String) checkKey(config, "mode", CommandEffect.DEFAULT_MODE.name());
    double chance = (double) checkKey(config, "chance", CommandEffect.DEFAULT_CHANCE);
    int interval = (int) checkKey(config, "interval", CommandEffect.DEFAULT_INTERVAL);
    int delay = (int) checkKey(config, "delay", CommandEffect.DEFAULT_DELAY);
    return new CommandEffect(commands, trigger, chance, interval, delay, groups, mode);
  }

}
