package me.ttimhcsnad.drugmeup.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.EffectData;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * The application tier DrugManager class Keeps track of {@link Drug}s loaded into memory Handles
 * interaction between model and UI classes for taking drugs
 */
public class DrugManager implements Manager {

  private DrugMeUp plugin;
  private UserManager userManager;
  private List<Drug> drugs;

  /**
   * Create a DrugManager
   *
   * @param plugin The referenced {@link DrugMeUp} instance
   */
  public DrugManager(DrugMeUp plugin) {
    this.plugin = plugin;
    this.userManager = plugin.getUserManager();
    this.drugs = new ArrayList<>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DrugManager init() {
    this.clearDrugs();
    return this;
  }

  /**
   * Add a {@link Drug} to the list of drugs
   *
   * @param drug The {@link Drug} being added
   */
  public void addDrug(Drug drug) {
    this.drugs.add(drug);
  }

  /**
   * Clears all drugs loaded into system previously
   */
  public void clearDrugs() {
    this.drugs.clear();
  }

  /**
   * Check whether an {@link ItemStack} is a valid {@link Drug}
   *
   * @param item The {@link ItemStack} being checked against
   * @return Whether the specified {@link ItemStack} is a {@link Drug}
   */
  public boolean isDrug(ItemStack item) {
    return this.getDrug(item) != null;
  }

  /**
   * Retrieve the instance of a {@link Drug} from a specified {@link ItemStack}
   *
   * @param item The {@link ItemStack} being referenced from
   * @return The {@link Drug} retrieved from the {@link ItemStack} (or null if not valid drug)
   */
  public Drug getDrug(ItemStack item) {
    return this.drugs.stream()
        .filter(drug -> drug.getMaterial() == item.getType())
        .filter(drug -> drug.getDurability() == item.getDurability())
        .filter(drug -> item.getEnchantments().isEmpty())
        .findFirst()
        .orElse(null);
  }

  /**
   * Call to make a specified {@link Player} take a {@link Drug}
   *
   * @param player The {@link Player} taking the {@link Drug}
   * @param drug The {@link Drug} being taken
   * @param item The {@link ItemStack} the {@link Drug} was taken from
   * @return A collection of {@link EffectData} that was gathered
   */
  public Collection<EffectData> takeDrug(Player player, Drug drug, ItemStack item) {
    Set<EffectData> gatheredEffects = new HashSet<>();

    // Create the DrugUser (if not exists)
    DrugUser user = userManager.addDrugUser(player);

    // Check for single-use drug setting
    if ((user.getCooldown() - System.currentTimeMillis()) <= 0) {

      // Choose a duration
      int duration = Utils.random(drug.getMaxDuration(), drug.getMinDuration());

      // Choose a group
      String group = drug.chooseRandomGroup();

      // Remove 1 of the item used
      item.setAmount(item.getAmount() - 1);

      // Update negative chances
      if (drug.getOdChance() > 0.00F) {
        user.setChanceOfOD((float) Utils.random((int) (drug.getOdChance() * 100), 1) / 100);
      }
      if (drug.getPukeChance() > 0.00F) {
        user.setChanceOfPuke((float) Utils.random((int) (drug.getPukeChance() * 100), 1) / 100);
      }
      if (drug.getFireChance() > 0.00F) {
        user.setChanceOfFire((float) Utils.random((int) (drug.getFireChance() * 100), 1) / 100);
      }
      if (drug.getHeartAttackChance() > 0.00F) {
        user.setChanceOfHeartAttack(
            (float) Utils.random((int) (drug.getHeartAttackChance() * 100), 1) / 100);
      }

      // Update comedowns before applying
      user.updateComedown(drug, duration);

      // Update cooldown
      if (drug.getCooldown() >= 0) {
        user.setCooldown(drug.getCooldown());
      } else {
        user.setCooldown(user.getTotalComedownTime());
      }

      // Apply all effects for the drug
      drug.getEffects().forEach(effect -> gatheredEffects
          .add(effect.gather(new EffectData(effect, user, drug, group, duration))));

      // Update applied effects
      user.addGatheredEffects(drug, gatheredEffects);

      return gatheredEffects;
    } else {
      String message = plugin.getFileManager().getConfig().getString("messages.cooldown");
      player.sendMessage(Utils.parseMessage(message, user));
    }

    return null;
  }
}
