package me.ttimhcsnad.drugmeup.managers;

/**
 * Represents a Manager Each manager is responsible for handling their own responsibilities
 */
public interface Manager {

  /**
   * Initialize the Manager Load essential components of the Manager
   *
   * @return The Manager instance, useful for chaining
   */
  Manager init();

}
