package me.ttimhcsnad.drugmeup.managers;

import static me.ttimhcsnad.drugmeup.Utils.checkKey;
import static me.ttimhcsnad.drugmeup.Utils.checkSection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.managers.factories.DrugFactory;
import me.ttimhcsnad.drugmeup.managers.factories.EffectFactory;
import me.ttimhcsnad.drugmeup.model.Drug;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.potion.PotionEffectType;

/**
 * The application tier FileManager class Keeps track of loading and accessing configuration files
 * associated with the Plugin
 */
public class FileManager implements Manager {

  /* constants for default config */
  public final static boolean DEFAULT_MUST_SNEAK = false;
  public final static boolean DEFAULT_RIGHT_CLICK = true;
  public final static boolean DEFAULT_LEFT_CLICK = false;
  public final static boolean DEFAULT_NEGATIVE_BROADCAST = true;
  public final static String DEFAULT_WORLDS = "*";
  public final static String DEFAULT_MSG_SOBER = "&eYou are beginning to feel sober.";
  public final static String DEFAULT_MSG_COOLDOWN = "&eYou must wait &c@times &ebefore &eusing drugs again!";
  public final static String DEFAULT_MSG_OVERDOSE = "&e@p &chas overdosed on @drug.";
  public final static String DEFAULT_MSG_CATCHFIRE = "&e@p &caccidentally burnt their eyelashes.";
  public final static String DEFAULT_MSG_PUKE = "&e@p &cpuked up their guts.";
  public final static String DEFAULT_MSG_HEARTATTACK = "&e@p &chad a heart attack on @drug.";

  private DrugMeUp plugin;
  private DrugManager drugManager;
  private FileConfiguration config;

  /**
   * Create a FileManager
   *
   * @param plugin The referenced {@link DrugMeUp} instance
   */
  public FileManager(DrugMeUp plugin) {
    this.plugin = plugin;
    this.drugManager = plugin.getDrugManager();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public FileManager init() {
    this.extractFromJar();
    this.saveLists();
    this.loadConfig();
    this.loadDrugConfigs();
    return this;
  }

  /**
   * Retrieve the stored instance of the plugin's configuration file
   *
   * @return The stored plugin's configuration file
   */
  public FileConfiguration getConfig() {
    return this.config;
  }

  /**
   * Save the default list of Bukkit items referenced in drug configurations
   */
  private void saveLists() {
    try {
      File itemFile = new File(plugin.getDataFolder() + "/ITEM_LIST.txt");
      File potionFile = new File(plugin.getDataFolder() + "/POTION_EFFECT_LIST.txt");
      File particleFile = new File(plugin.getDataFolder() + "/PARTICLE_EFFECT_LIST.txt");
      File soundFile = new File(plugin.getDataFolder() + "/SOUND_EFFECT_LIST.txt");
      BufferedWriter bw;
      String NAMESPACE = "minecraft:";

      bw = new BufferedWriter(new FileWriter(itemFile));
      for (Material type : Material.values()) {
        bw.write(NAMESPACE + type.name());
        bw.newLine();
      }
      bw.close();

      bw = new BufferedWriter(new FileWriter(potionFile));
      for (PotionEffectType type : PotionEffectType.values()) {
        if (type != null) {
          bw.write(NAMESPACE + type.getName());
          bw.newLine();
        }
      }
      bw.close();

      bw = new BufferedWriter(new FileWriter(particleFile));
      for (Particle particle : Particle.values()) {
        bw.write(NAMESPACE + particle.name());
        bw.newLine();
      }
      bw.close();

      bw = new BufferedWriter(new FileWriter(soundFile));
      for (Sound sound : Sound.values()) {
        bw.write(NAMESPACE + sound.name());
        bw.newLine();
      }
      bw.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  /**
   * Extracts all of the default drug configurations from the jar file
   */
  private void extractFromJar() {
    try {
      Path drugsPath = FileSystems.getDefault().getPath(plugin.getDataFolder() + "/drugs");
      if (!Files.exists(drugsPath)) {
        Files.createDirectory(drugsPath);
        plugin.log.info("Loading Default Drug Configs...");
        final File jarFile = new File(
            getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        final JarFile jar = new JarFile(jarFile);
        final Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements()) {
          final String name = entries.nextElement().getName();
          if (name.startsWith("drugs/")) {
            plugin.saveResource(name, false);
          }
        }
        jar.close();
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Saves the default configuration file and stores in memory
   */
  private void loadConfig() {
    try {
      File configFile = FileSystems.getDefault()
          .getPath(plugin.getDataFolder() + "/config.yml").toFile();
      config = YamlConfiguration.loadConfiguration(configFile);
      checkKey(config, "worlds", DEFAULT_WORLDS);
      checkKey(config, "must-sneak", DEFAULT_MUST_SNEAK);
      checkKey(config, "right-click", DEFAULT_RIGHT_CLICK);
      checkKey(config, "left-click", DEFAULT_LEFT_CLICK);
      checkSection(config, "messages");
      checkKey(config, "messages.sober", DEFAULT_MSG_SOBER);
      checkKey(config, "messages.cooldown", DEFAULT_MSG_COOLDOWN);
      checkSection(config, "messages.negatives");
      checkKey(config, "messages.negatives.broadcast", DEFAULT_NEGATIVE_BROADCAST);
      checkKey(config, "messages.negatives.overdose", DEFAULT_MSG_OVERDOSE);
      checkKey(config, "messages.negatives.catchfire", DEFAULT_MSG_CATCHFIRE);
      checkKey(config, "messages.negatives.puke", DEFAULT_MSG_PUKE);
      checkKey(config, "messages.negatives.heartattack", DEFAULT_MSG_HEARTATTACK);
      config.save(configFile);
    } catch (Exception ex) {
      plugin.log.warning(ex.getMessage() + " in config.yml");
    }
  }

  /**
   * Load all of the drug configurations from the plugin data folder
   */
  private void loadDrugConfigs() {
    try {
      Files.list(FileSystems.getDefault().getPath(plugin.getDataFolder() + "/drugs/"))
          .forEach(path -> {
            YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(path.toFile());
            this.loadDrugConfig(path.toFile(), drugConfig);
          });
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Load the specified drug configuration
   *
   * @param file The {@link Path} to the drug configuration file
   * @param config The {@link YamlConfiguration} instance of the drug config
   */
  private void loadDrugConfig(File file, YamlConfiguration config) {
    try {
      Drug drug = DrugFactory.loadDrug(config);
      drugManager.addDrug(drug);
      Utils.logColor(plugin, "Loaded " + drug);
      config.save(file);
    } catch (Exception ex) {
      plugin.log.warning(ex.getMessage() + " for config '" + file.getName() + "'.");
    }
  }

  /**
   * Create a drug config with specified values and some default configured values
   *
   * @param drugName The name of the drug being created
   * @param item The namespaced material name for the drug
   * @throws Exception Throws IOException in case of filesystem error Throws Exception in case of
   * invalid material name
   */
  public void setupDrug(String drugName, String item) throws Exception {
    try {
      String parsedName = item.replaceAll(Pattern.quote(".") + ".*", "")
          .replaceAll("minecraft:", "").toUpperCase();
      if (existsInArray(Material.values(), Material.matchMaterial(parsedName))) {
        String coloredName = ChatColor.translateAlternateColorCodes('&', drugName);
        String strippedName = ChatColor.stripColor(coloredName).toLowerCase();
        Path filePath = FileSystems.getDefault()
            .getPath(plugin.getDataFolder() + "/drugs/" + strippedName + ".yml");
        Files.createFile(filePath);

        // Load default drug config
        YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(filePath.toFile());
        DrugFactory.loadDrug(drugConfig, drugName, item);
        drugConfig.save(filePath.toFile());
      } else {
        throw new Exception(item + " is not a valid item name!");
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Create a potion effect for the specified drug with default values
   *
   * @param drugName The name of the drug config file being referenced
   * @param name The name of the potion effect
   * @throws Exception Throws IOException in case of filesystem error Throws Exception in case of
   * invalid potion effect name name
   */
  public void addPotionEffect(String drugName, String name) throws Exception {
    Path filePath = FileSystems.getDefault()
        .getPath(plugin.getDataFolder() + "/drugs/" + drugName.toLowerCase() + ".yml");
    if (filePath.toFile().exists()) {
      YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(filePath.toFile());

      // Get potion effect names in own list
      PotionEffectType[] types = PotionEffectType.values();
      List<String> typeNames = new ArrayList<>();
      for (PotionEffectType type : types) {
        if (type != null) {
          typeNames.add(type.getName());
        }
      }

      if (typeNames.contains(name.replaceAll("minecraft:", "").toUpperCase())) {
        ConfigurationSection potionSection = drugConfig.getConfigurationSection("effects.potions");
        potionSection.createSection(name.toLowerCase());
        ConfigurationSection effectSection = potionSection.getConfigurationSection(name);
        EffectFactory.loadPotionEffect(effectSection);
        drugConfig.save(filePath.toFile());
      } else {
        throw new Exception(name + " is not a valid potion type!");
      }
    } else {
      throw new FileNotFoundException("A config for " + drugName + " was not found!");
    }
  }

  /**
   * Create a particle effect for the specified drug with default values
   *
   * @param drugName The name of the drug config file being referenced
   * @param name The name of the particle effect
   * @throws Exception Throws IOException in case of filesystem error Throws Exception in case of
   * invalid particle effect name name
   */
  public void addParticleEffect(String drugName, String name) throws Exception {
    Path filePath = FileSystems.getDefault()
        .getPath(plugin.getDataFolder() + "/drugs/" + drugName.toLowerCase() + ".yml");
    if (filePath.toFile().exists()) {
      YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(filePath.toFile());

      Particle[] types = Particle.values();
      List<String> typeNames = new ArrayList<>();
      for (Particle type : types) {
        if (type != null) {
          typeNames.add(type.name());
        }
      }

      // Get particle effect names in own list
      if (typeNames.contains(name.replaceAll("minecraft:", "").toUpperCase())) {
        ConfigurationSection particleSection = drugConfig
            .getConfigurationSection("effects.particles");
        particleSection.createSection(name.toLowerCase());
        ConfigurationSection effectSection = particleSection.getConfigurationSection(name);
        EffectFactory.loadParticleEffect(effectSection);
        drugConfig.save(filePath.toFile());
      } else {
        throw new Exception(name + " is not a valid particle type!");
      }
    } else {
      throw new FileNotFoundException("A config for " + drugName + " was not found!");
    }
  }

  /**
   * Create a sound effect for the specified drug with default values
   *
   * @param drugName The name of the drug config file being referenced
   * @param name The name of the potion effect
   * @throws Exception Throws IOException in case of filesystem error Throws Exception in case of
   * invalid sound effect name name
   */
  public void addSoundEffect(String drugName, String name) throws Exception {
    Path filePath = FileSystems.getDefault()
        .getPath(plugin.getDataFolder() + "/drugs/" + drugName.toLowerCase() + ".yml");
    if (filePath.toFile().exists()) {
      YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(filePath.toFile());

      Sound[] types = Sound.values();
      List<String> typeNames = new ArrayList<>();
      for (Sound type : types) {
        if (type != null) {
          typeNames.add(type.name());
        }
      }

      // Get sound effect names in own list
      if (typeNames.contains(name.replaceAll("minecraft:", "").toUpperCase())) {
        ConfigurationSection soundSection = drugConfig.getConfigurationSection("effects.sounds");
        soundSection.createSection(name.toLowerCase());
        ConfigurationSection effectSection = soundSection.getConfigurationSection(name);
        EffectFactory.loadSoundEffect(effectSection);
        drugConfig.save(filePath.toFile());
      } else {
        throw new Exception(name + " is not a valid particle type!");
      }
    } else {
      throw new FileNotFoundException("A config for " + drugName + " was not found!");
    }
  }

  /**
   * Create a command effect for the specified drug with default values
   *
   * @param drugName The name of the drug config file being referenced
   * @param command The command being added to the commands list for the effect
   * @throws Exception Throws FileNotFoundException in case of filesystem error
   */
  public void addCommand(String drugName, String command) throws Exception {
    Path filePath = FileSystems.getDefault()
        .getPath(plugin.getDataFolder() + "/drugs/" + drugName.toLowerCase() + ".yml");
    if (filePath.toFile().exists()) {
      YamlConfiguration drugConfig = YamlConfiguration.loadConfiguration(filePath.toFile());

      // Get potion effect names in own list
      ConfigurationSection commandSection = drugConfig.getConfigurationSection("effects.commands");
      String section = String.valueOf(UUID.randomUUID());
      commandSection.createSection(section);
      ConfigurationSection effectSection = commandSection.getConfigurationSection(section);
      List<String> commands = Collections.singletonList(command);
      EffectFactory.loadCommandEffect(effectSection, commands);
      effectSection.set("commands", commands);
      drugConfig.save(filePath.toFile());
    } else {
      throw new FileNotFoundException("A config for " + drugName + " was not found!");
    }
  }

  /**
   * Helper function to check if a value exists in an array
   *
   * @param values The array of value
   * @param value The value being checked
   * @return Whether the value exists in the values array
   */
  private boolean existsInArray(Object[] values, Object value) {
    return Arrays.asList(values).contains(value);
  }

}
