package me.ttimhcsnad.drugmeup.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.DelayableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.LoopableEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.NegativeEffect;
import me.ttimhcsnad.drugmeup.model.effects.interfaces.TimedEffect;
import me.ttimhcsnad.drugmeup.model.enums.TriggerTime;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * The application tier UserManager class Keeps track of {@link DrugUser}s entering and exiting the
 * system Maintains primary scheduled task for dispatching effects and handling sobriety
 */
public class UserManager implements Manager {

  private Plugin plugin;
  private List<DrugUser> drugUsers;
  private Integer userTimer = null;

  private final long NEG_WEAROFF_INTERVAL = 5000L;
  private final float NEG_WEAROFF_DELTA = 0.75F;

  /**
   * Create a UserManager
   *
   * @param plugin The referenced {@link DrugMeUp} instance
   */
  public UserManager(Plugin plugin) {
    this.plugin = plugin;
    this.drugUsers = new ArrayList<>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UserManager init() {
    this.soberAll();
    if (this.userTimer != null) {
      Bukkit.getScheduler().cancelTask(this.userTimer);
    }
    this.userTimer = this.startUserTimer();
    return this;
  }

  /**
   * Start the main {@link org.bukkit.scheduler.BukkitScheduler} task for DMU tasks
   *
   * @return The ID for the task started
   */
  private int startUserTimer() {
    return Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, () -> {
      // Check comedown times for drug users
      for (DrugUser drugUser : new ArrayList<>(drugUsers)) {

        // Reduce all negative chances at specified interval for specified delta
        if (drugUser.getNextWearoff() < System.currentTimeMillis()) {
          drugUser.setNextWearoff(System.currentTimeMillis() + NEG_WEAROFF_INTERVAL);
          drugUsers.forEach(user -> user.setAllNegChances(NEG_WEAROFF_DELTA));
        }

        // Check if the user is going to comedown
        drugUser.checkSober();

        // Check if the user has comedown from all drugs
        if (!drugUser.hasActiveDrugs()) {
          drugUsers.remove(drugUser);
        }

        // Run non-delayed regular effects
        drugUser.getAllGatheredEffects().stream()
            .filter(Objects::nonNull)
            .filter(effectData -> effectData.getEffect() != null)
            .filter(effectData -> (!(effectData.getEffect() instanceof LoopableEffect)
                || !((LoopableEffect) effectData.getEffect()).doesLoop()))
            .filter(effectData -> (!(effectData.getEffect() instanceof DelayableEffect)
                || !((DelayableEffect) effectData.getEffect()).isDelayed()))
            .filter(effectData -> (!(effectData.getEffect() instanceof TimedEffect)
                || ((TimedEffect) effectData.getEffect()).getTriggerTime() == TriggerTime.START))
            .filter(effectData -> !(effectData.getEffect() instanceof NegativeEffect))
            .forEach(effectData -> {
              effectData.getEffect().apply(effectData);
              drugUser.removeGatheredEffect(effectData.getDrug(), effectData);
              drugUser.addAppliedEffect(effectData.getDrug(), effectData);
            });

        // Run single negative effect
        drugUser.getAllGatheredEffects().stream()
            .filter(Objects::nonNull)
            .filter(effectData -> effectData.getEffect() != null)
            .filter(effectData -> effectData.getEffect() instanceof NegativeEffect)
            .findFirst()
            .ifPresent(effectData -> {
              effectData.getEffect().apply(effectData);
              drugUser.removeGatheredEffect(effectData.getDrug(), effectData);
              drugUser.addAppliedEffect(effectData.getDrug(), effectData);
            });

        // Run delayed effects
        drugUser.getAllGatheredEffects().stream()
            .filter(Objects::nonNull)
            .filter(effectData -> effectData.getEffect() != null)
            .filter(effectData -> !(effectData.getEffect() instanceof LoopableEffect)
                || !((LoopableEffect) effectData.getEffect()).doesLoop())
            .filter(effectData -> effectData.getEffect() instanceof DelayableEffect)
            .filter(effectData -> ((DelayableEffect) effectData.getEffect()).isDelayed())
            .filter(effectData -> drugUser.getDelayedEffectTime(
                (DelayableEffect) effectData.getEffect()) >= 0)
            .filter(effectData -> drugUser.getDelayedEffectTime(
                (DelayableEffect) effectData.getEffect()) < System.currentTimeMillis())
            .forEach(effectData -> {
              effectData.getEffect().apply(effectData);
              drugUser.removeGatheredEffect(effectData.getDrug(), effectData);
              drugUser.addAppliedEffect(effectData.getDrug(), effectData);
            });

        // Apply all looping effects
        drugUser.getAllGatheredEffects().stream()
            .filter(Objects::nonNull)
            .filter(effectData -> effectData.getEffect() != null)
            .filter(effectData -> effectData.getEffect() instanceof LoopableEffect)
            .filter(effectData -> ((LoopableEffect) effectData.getEffect()).doesLoop())
            .filter(effectData -> !(effectData.getEffect() instanceof DelayableEffect)
                || !((DelayableEffect) effectData.getEffect()).isDelayed()
                || drugUser.getDelayedEffectTime(
                (DelayableEffect) effectData.getEffect()) < System.currentTimeMillis())
            .filter(effectData -> drugUser.getNextEffectInterval(
                (LoopableEffect) effectData.getEffect()) < System.currentTimeMillis())
            .forEach(effectData -> {
              effectData.getEffect().apply(effectData);
              drugUser.setNextEffectInterval((LoopableEffect) effectData.getEffect(),
                  ((LoopableEffect) effectData.getEffect()).getInterval());
            });
      }
    }, 0L, 1L);
  }

  /**
   * Sober all active {@link DrugUser}'s
   */
  public void soberAll() {
    drugUsers.forEach(DrugUser::soberAll);
  }

  /**
   * Setup a player as a {@link DrugUser} This will return the active {@link DrugUser} instance if
   * one already exists
   *
   * @param player The {@link Player} being registered as a {@link DrugUser}
   * @return The {@link DrugUser} registered
   */
  public DrugUser addDrugUser(Player player) {
    DrugUser user = this.getDrugUser(player);
    if (user == null) {
      this.drugUsers.add(user = new DrugUser(player));
    }
    return user;
  }

  /**
   * Retrieve a {@link DrugUser} instance from the {@link Player} they were registered with
   *
   * @param player The {@link Player} the {@link DrugUser} was registered with
   * @return The requested {@link DrugUser} or null if non-existent
   */
  public DrugUser getDrugUser(Player player) {
    return this.drugUsers.stream()
        .filter(drugUser -> player.getUniqueId().equals(drugUser.getUniqueId()))
        .findFirst()
        .orElse(null);
  }

}
