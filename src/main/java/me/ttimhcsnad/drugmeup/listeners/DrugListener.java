package me.ttimhcsnad.drugmeup.listeners;

import java.util.Arrays;
import me.ttimhcsnad.drugmeup.DrugMeUp;
import me.ttimhcsnad.drugmeup.Utils;
import me.ttimhcsnad.drugmeup.events.DrugTakenEvent;
import me.ttimhcsnad.drugmeup.events.NegativeEffectEvent;
import me.ttimhcsnad.drugmeup.events.SoberEvent;
import me.ttimhcsnad.drugmeup.managers.DrugManager;
import me.ttimhcsnad.drugmeup.managers.FileManager;
import me.ttimhcsnad.drugmeup.managers.UserManager;
import me.ttimhcsnad.drugmeup.model.Drug;
import me.ttimhcsnad.drugmeup.model.DrugUser;
import me.ttimhcsnad.drugmeup.model.effects.negatives.CatchFireEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.HeartAttackEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.ODEffect;
import me.ttimhcsnad.drugmeup.model.effects.negatives.PukeEffect;
import me.ttimhcsnad.drugmeup.model.enums.TriggerType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 * Establishes a {@link Listener} for handling {@link org.bukkit.event.Event}s
 */
@SuppressWarnings("unused")
public class DrugListener implements Listener {

  private DrugMeUp plugin;
  private DrugManager drugManager;
  private UserManager userManager;
  private FileManager fileManager;

  /**
   * Create a DrugListener Handles {@link org.bukkit.event.Event}s that are thrown Handles custom
   * events thrown in {@link DrugMeUp}
   *
   * @param plugin The current {@link DrugMeUp} plugin instance
   */
  public DrugListener(DrugMeUp plugin) {
    this.plugin = plugin;
    this.drugManager = plugin.getDrugManager();
    this.userManager = plugin.getUserManager();
    this.fileManager = plugin.getFileManager();
  }

  /**
   * Helper method to check if the {@link Player} is sneaking and must be sneaking to take drugs
   *
   * @param player The {@link Player} being used to check if they are sneaking
   * @return Whether they are sneaking and must be sneaking
   */
  private boolean checkSneak(Player player) {
    return ((fileManager.getConfig().getBoolean("must-sneak") && player.isSneaking())
        || !fileManager.getConfig().getBoolean("must-sneak"));
  }

  /**
   * Helper method to check if the {@link Player} is in an allowed world when taking drugs
   *
   * @param player The {@link Player} being used to check the world they are in
   * @return Whether they are in an allowed world
   */
  private boolean checkWorld(Player player) {
    return (
        Arrays.asList(fileManager.getConfig().getString("worlds").replaceAll(" ", "").split(","))
            .contains(player.getWorld().getName())
            || fileManager.getConfig().getString("worlds").equals("*"));
  }

  /**
   * Helper method to check if the action passed to the {@link PlayerInteractEvent} is valid based
   * on options set in the config
   *
   * @param event The {@link PlayerInteractEvent} being checked
   * @return Whether the action was valid
   */
  private boolean checkAction(PlayerInteractEvent event) {
    boolean shouldExecute = false;
    if (fileManager.getConfig().getBoolean("right-click")) {
      shouldExecute = event.getAction() == Action.RIGHT_CLICK_AIR
          || event.getAction() == Action.RIGHT_CLICK_BLOCK;
    }
    if (fileManager.getConfig().getBoolean("left-click")) {
      shouldExecute = shouldExecute
          || event.getAction() == Action.LEFT_CLICK_AIR
          || event.getAction() == Action.LEFT_CLICK_BLOCK;
    }
    return shouldExecute;
  }

  /**
   * Catch event thrown when {@link Player} consumes a {@link Drug} with {@link TriggerType} USE
   * Handles taking {@link Drug} and throws {@link DrugTakenEvent} on successful take
   *
   * @param event The {@link PlayerInteractEvent} being caught
   */
  @EventHandler
  public void onDrugUse(PlayerInteractEvent event) {
    Player player = event.getPlayer();
    if (this.checkAction(event)) {
      if (event.getHand() == EquipmentSlot.HAND) {
        ItemStack item = player.getInventory().getItemInMainHand();
        if (item != null && item.getType() != Material.AIR) {
          if (this.drugManager.isDrug(item)) {
            if (this.checkSneak(player) && this.checkWorld(player)) {
              Drug drug = this.drugManager.getDrug(item);
              if (drug.getTrigger() == TriggerType.USE) {
                if (player.hasPermission("drugmeup.use")) {
                  Bukkit.getServer().getPluginManager().callEvent(
                      new DrugTakenEvent(
                          player,
                          drug,
                          this.drugManager.takeDrug(player, drug, item)
                      )
                  );
                } else {
                  Utils.noPermission(player);
                }
                event.setCancelled(true);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Catch event thrown when {@link Player} consumes a {@link Drug} with {@link TriggerType} CONSUME
   * Handles taking {@link Drug} and throws {@link DrugTakenEvent} on successful take
   *
   * @param event The {@link PlayerItemConsumeEvent} being caught
   */
  @EventHandler
  public void onDrugConsume(PlayerItemConsumeEvent event) {
    if (event.isCancelled()) {
      return;
    }
    Player player = event.getPlayer();
    ItemStack item = event.getItem();
    if (drugManager.isDrug(item)) {
      Drug drug = this.drugManager.getDrug(item);
      if (drug.getTrigger() == TriggerType.CONSUME) {
        if (this.checkWorld(player)) {
          if (player.hasPermission("drugmeup.use")) {
            Bukkit.getServer().getPluginManager().callEvent(
                new DrugTakenEvent(
                    player,
                    drug,
                    this.drugManager.takeDrug(player, drug, item)
                )
            );
          } else {
            Utils.noPermission(player);
          }
        }
      }
    }
  }

  /**
   * Catch event thrown when {@link Player} drinks milk Handles sobering the {@link DrugUser}
   *
   * @param event The {@link PlayerItemConsumeEvent} being caught
   */
  @EventHandler
  public void onMilkConsume(PlayerItemConsumeEvent event) {
    Player player = event.getPlayer();
    ItemStack item = event.getItem();
    DrugUser user = userManager.getDrugUser(player);
    if (user != null) {
      if (item.getType() == Material.MILK_BUCKET) {
        user.soberAll();
      }
    }
  }

  /**
   * Catch event thrown when {@link Player} dies Handles sobering user
   *
   * @param event The {@link PlayerDeathEvent} being caught
   */
  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent event) {
    Player player = event.getEntity();
    DrugUser user = this.userManager.getDrugUser(player);
    if (user != null) {
      user.soberAll();
    }
  }

  /**
   * Catch event thrown when {@link Player} leaves sever Handles sobering user
   *
   * @param event The {@link PlayerQuitEvent} being caught
   */
  @EventHandler
  public void onQuit(PlayerQuitEvent event) {
    Player player = event.getPlayer();
    DrugUser user = this.userManager.getDrugUser(player);
    if (user != null) {
      user.soberAll();
    }
  }

  /**
   * Catch event thrown when a {@link DrugUser} sobers from a {@link Drug} Handles sending sober
   * messages
   *
   * @param event The {@link SoberEvent} being caught
   */
  @EventHandler
  public void onSober(SoberEvent event) {
    Player player = event.getUser().getPlayer();
    if (!event.getUser().hasActiveDrugs()) {
      String message = fileManager.getConfig().getString("messages.sober");
      if (message != null && !message.equals("")) {
        player.sendMessage(Utils.parseMessage(message, player, event.getDrug()));
      }
    }
  }

  /**
   * Catch event thrown when a negative effect occurs Handles sending/broadcasting messages
   *
   * @param event The {@link NegativeEffectEvent} being caught
   */
  @EventHandler
  public void onNegativeEffect(NegativeEffectEvent event) {
    Player player = event.getDrugUser().getPlayer();
    String type = "";
    type = (event.getEffect() instanceof ODEffect) ? "overdose" : type;
    type = (event.getEffect() instanceof CatchFireEffect) ? "catchfire" : type;
    type = (event.getEffect() instanceof PukeEffect) ? "puke" : type;
    type = (event.getEffect() instanceof HeartAttackEffect) ? "heartattack" : type;
    boolean broadcast = fileManager.getConfig().getBoolean("messages.negatives.broadcast");
    String message = fileManager.getConfig().getString("messages.negatives." + type);
    if (message != null && !message.equals("")) {
      message = Utils.parseMessage(message, player, event.getDrug());
      if (broadcast) {
        Bukkit.getServer().broadcastMessage(message);
      } else {
        player.sendMessage(message);
      }
    }
  }

  /**
   * Catch event thrown when a player takes a {@link Drug} Handles sending random drug message to
   * {@link Player}
   *
   * @param event The {@link DrugTakenEvent} being caught
   */
  @EventHandler
  public void onTakeDrug(DrugTakenEvent event) {
    if (!event.isCancelled()) {
      Player player = event.getPlayer();
      Drug drug = event.getDrug();
      String message = drug.getRandomUseMessage();
      player.sendMessage(Utils.parseMessage(message, player, drug));
    }
  }

}
