package me.ttimhcsnad.drugmeup;

import java.util.logging.Logger;
import me.ttimhcsnad.drugmeup.handlers.DMUCommandHandler;
import me.ttimhcsnad.drugmeup.handlers.SoberCommandHandler;
import me.ttimhcsnad.drugmeup.handlers.TabCompleteHandler;
import me.ttimhcsnad.drugmeup.listeners.DrugListener;
import me.ttimhcsnad.drugmeup.managers.DrugManager;
import me.ttimhcsnad.drugmeup.managers.FileManager;
import me.ttimhcsnad.drugmeup.managers.UserManager;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class DrugMeUp extends JavaPlugin {

  public final Logger log = getLogger();
  private UserManager userManager;
  private DrugManager drugManager;
  private FileManager fileManager;

  /**
   * Enable DrugMeUp Loads essential application-tier components
   */
  @Override
  public void onEnable() {
    userManager = new UserManager(this).init();
    drugManager = new DrugManager(this).init();
    fileManager = new FileManager(this).init();
    this.registerListeners();
    this.registerCommands();
    log.info("Enabled!");
  }

  /**
   * Disable DrugMeUp Sober's all active {@link me.ttimhcsnad.drugmeup.model.DrugUser}s
   */
  @Override
  public void onDisable() {
    userManager.soberAll();
    log.info("Disabled!");
  }

  /**
   * Reload DrugMeUp Re-initialized the application-tier components
   */
  public void reload() {
    drugManager.init();
    userManager.init();
    fileManager.init();
  }

  /**
   * Register all events being listened for
   */
  private void registerListeners() {
    this.getServer().getPluginManager().registerEvents(new DrugListener(this), this);
  }

  /**
   * Register command setting executor and tab completer
   */
  private void registerCommands() {
    PluginCommand dmuCommand = this.getCommand("drugmeup");
    PluginCommand soberCommand = this.getCommand("sober");
    dmuCommand.setExecutor(new DMUCommandHandler(this));
    dmuCommand.setTabCompleter(new TabCompleteHandler(this));
    soberCommand.setExecutor(new SoberCommandHandler(this));
  }

  /**
   * Retrieve the current instance of DrugMeUp Plugin
   *
   * @return The current instance of DrugMeUp plugin
   */
  @SuppressWarnings("unused")
  public DrugMeUp getInstance() {
    return this;
  }

  /**
   * Retrieve a static reference to the active {@link DrugManager} class This allows hooks to
   * manage/poll drug states within the plugin
   *
   * @return The current instance of {@link DrugManager}
   */
  @SuppressWarnings("unused")
  public DrugManager getDrugManager() {
    return drugManager;
  }

  /**
   * Retrieve a static reference to the active {@link FileManager} class This allows hooks to manage
   * file interactions in the plugin
   *
   * @return The current instance of {@link FileManager}
   */
  @SuppressWarnings("unused")
  public FileManager getFileManager() {
    return fileManager;
  }

  /**
   * Retrieve a static reference to the active {@link UserManager} class This allows hooks to
   * manage/poll {@link me.ttimhcsnad.drugmeup.model.DrugUser} states within the plugin
   *
   * @return The current instance of {@link UserManager}
   */
  @SuppressWarnings("unused")
  public UserManager getUserManager() {
    return userManager;
  }

}
